/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.StatementTree;
import java.lang.annotation.Annotation;
import java.util.Objects;
import java.util.Optional;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;
import org.vermeerlab.apt.ValidationResult;
import org.vermeerlab.apt.ValidationResultDetail;
import org.vermeerlab.compiler.CompilerException;
import org.vermeerlab.compiler.SupplyMethodExecutor;
import org.vermeerlab.compiler.SupplyMethodInterface;

/**
 * 分類が{@link javax.lang.model.element.ElementKind#METHOD} のElementを扱う実装です.
 *
 * @author Yamashita,Takahiro
 */
public class MethodElement implements CategorizedElementInterface, SupplyMethodInterface {

    final BaseElement baseElement;

    /**
     * For extention
     */
    protected MethodElement() {
        this.baseElement = null;
    }

    protected MethodElement(BaseElement baseElement) {
        this.baseElement = baseElement;
    }

    protected <T extends MethodElement> MethodElement(T methodElement) {
        this.baseElement = methodElement.baseElement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean canInstance(ElementKind elementKind) {
        return elementKind == ElementKind.METHOD;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public <R extends CategorizedElementInterface> Optional<R> create(BaseElement baseElement) {
        return this.canInstance(baseElement.getElement().getKind())
               ? Optional.of((R) new MethodElement(baseElement))
               : Optional.empty();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<Class<? extends Annotation>> getTargetAnnotation() {
        return this.baseElement.getTargetAnnotation();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <A extends Annotation> Optional<A> getAnnotation(Class<A> annotationType) {
        return this.baseElement.getAnnotation(annotationType);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Element getElement() {
        return this.baseElement.getElement();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getPackagePath() {
        return this.baseElement.getPackagePath();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getSimpleName() {
        return this.baseElement.getSimpleName();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R children(Class<? extends Annotation> annotation) {
        return this.baseElement.children(annotation);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R children() {
        return this.baseElement.children();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMethodBlock() {
        MethodTree methodTree = (MethodTree) this.baseElement.processingEnvHelper.getTree(this.getElement());
        BlockTree blockTree = methodTree.getBody();
        StringBuilder sb = new StringBuilder();
        for (StatementTree statementTree : blockTree.getStatements()) {
            sb.append(statementTree.toString());
        }
        return sb.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TypeMirror getReturnType() {
        ExecutableElement ee = (ExecutableElement) this.baseElement.getElement();
        TypeMirror returnType = ee.getReturnType();
        return returnType;
    }

    /**
     * 要素クラスの完全修飾名を返却します.
     *
     * @return 要素の完全修飾名
     */
    public String getQualifiedName() {
        return this.getPackagePath() + "." + this.getElement().getEnclosingElement().getSimpleName().toString();
    }

    /**
     * 注釈したメソッドの実行結果を返却します.
     * <P>
     * 戻り値は呼出元で用途に応じてキャストをしてください.
     *
     * @return 対象メソッドの実行結果
     * @throws CompilerException コードコンパイル時にエラーがあった場合
     */
    public Object value() {
        return SupplyMethodExecutor.of(this).toReturnResult();
    }

    /**
     * Elementの型が等しい、もしくは継承しているか判定します.
     * <P>
     * 継承元も含めて検証を行います.
     *
     * @param clazz 検証する型
     * @return 等しい、もしくは継承をしている場合 true
     */
    public Boolean isSameType(Class<?> clazz) {
        return this.baseElement.processingEnvHelper.isSameType(this.getReturnType(), clazz);
    }

    /**
     * 戻り値の型を検証します.
     * <P>
     * Elementの型が等しい、もしくは継承していることを検証します.
     *
     * @param clazz 検証対象の型
     * @return 検証結果.Elementの型が等しい、もしくは継承していない場合、エラー
     */
    public ValidationResult validateReturnTypeIsSame(Class<?> clazz) {
        ValidationResult result = ValidationResult.create();

        if (this.isSameType(clazz) == false) {

            String targetAnnotationName = this.baseElement.getTargetAnnotation().isPresent()
                                          ? this.baseElement.getTargetAnnotation().get().getSimpleName()
                                          : "";

            result.append(ValidationResultDetail.of(
                    targetAnnotationName + " annotate method return type invalid. it must be " + clazz.getCanonicalName(),
                    this.baseElement.getElement()
            ));
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.baseElement);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MethodElement other = (MethodElement) obj;
        return Objects.equals(this.baseElement, other.baseElement);
    }
}
