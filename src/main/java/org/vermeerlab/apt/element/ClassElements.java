/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.lang.model.element.ElementKind;

/**
 * 分類が{@link javax.lang.model.element.ElementKind#CLASS} の分類済みElementリストの集約を扱う実装です.
 *
 * @author Yamashita,Takahiro
 */
public class ClassElements implements CategorizedElementsInterface {

    final BaseElements baseElements;

    /**
     * For extention
     */
    protected ClassElements() {
        this.baseElements = null;
    }

    protected ClassElements(BaseElements baseElements) {
        this.baseElements = baseElements;
    }

    protected <T extends ClassElements> ClassElements(T classElements) {
        this.baseElements = classElements.baseElements;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<Class<? extends Annotation>> getTargetAnnotation() {
        return this.baseElements.getTargetAnnotation();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked")
    public Optional<? extends CategorizedElementsInterface> create(BaseElements baseElements) {
        if (CategorizedElementKind.CLASS.isAllAssignableFrom(baseElements)) {
            return Optional.of(this.toCategorizedElements(baseElements));
        }
        return Optional.empty();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked") //ジェネリック
    public <R extends CategorizedElementsInterface> R toCategorizedElements(BaseElements baseElements) {
        return (R) new ClassElements(baseElements);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked") //ジェネリック
    public List<? extends ClassElement> values() {
        return (List<? extends ClassElement>) this.baseElements.values();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Boolean isEmpty() {
        return this.baseElements.isEmpty();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R filter(ElementKind kind) {
        return this.baseElements.filter(kind);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.baseElements);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClassElements other = (ClassElements) obj;
        return Objects.equals(this.baseElements, other.baseElements);
    }

}
