/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.Objects;
import java.util.Optional;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;
import org.vermeerlab.apt.AnnotationProcessorException;
import org.vermeerlab.apt.ValidationResult;
import org.vermeerlab.apt.ValidationResultDetail;

/**
 * 分類が{@link javax.lang.model.element.ElementKind#FIELD} のElementを扱う実装です.
 * <P>
 * @author Yamashita,Takahiro
 */
public class FieldElement implements CategorizedElementInterface {

    final BaseElement baseElement;

    /**
     * For extention
     */
    protected FieldElement() {
        baseElement = null;
    }

    protected FieldElement(BaseElement baseElement) {
        this.baseElement = baseElement;
    }

    protected <T extends FieldElement> FieldElement(T fieldElement) {
        this.baseElement = fieldElement.baseElement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean canInstance(ElementKind elementKind) {
        return elementKind == ElementKind.FIELD;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public <R extends CategorizedElementInterface> Optional<R> create(BaseElement baseElement) {
        return this.canInstance(baseElement.getElement().getKind())
               ? Optional.of((R) new FieldElement(baseElement))
               : Optional.empty();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<Class<? extends Annotation>> getTargetAnnotation() {
        return this.baseElement.getTargetAnnotation();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <A extends Annotation> Optional<A> getAnnotation(Class<A> annotationType) {
        return this.baseElement.getAnnotation(annotationType);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Element getElement() {
        return this.baseElement.getElement();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getPackagePath() {
        return this.baseElement.getPackagePath();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getSimpleName() {
        return this.baseElement.getSimpleName();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R children(Class<? extends Annotation> annotation) {
        return this.baseElement.children(annotation);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R children() {
        return this.baseElement.children();
    }

    /**
     * 要素クラスの完全修飾名を返却します.
     *
     * @return 要素の完全修飾名
     */
    public String getQualifiedName() {
        return this.getPackagePath() + "." + this.baseElement.getElement().getEnclosingElement().getSimpleName().toString();
    }

    /**
     * フィールド値を返却します.
     *
     * @return フィールドの値.値が設定されていない {@code null}）の場合もありえます.
     */
    public Optional<String> getConstantValue() {
        validateModifierFinal();
        VariableElement ve = (VariableElement) this.baseElement.getElement();
        return ve.getConstantValue() == null ? Optional.empty() : Optional.of(ve.getConstantValue().toString());
    }

    /**
     * フィールドが{@code final} であることを検証します.
     * <P>
     * 注釈したフィールドの値を取得する場合、対象のフィールドは {@code final} でなければなりません.
     *
     * @return 検証結果.対象のフィールドが {@code final} で無い場合は検証不正.
     */
    void validateModifierFinal() {
        ValidationResult result = ValidationResult.create();

        if (this.isValidModifierFinal() == false) {
            String targetAnnotationName = this.baseElement.getTargetAnnotation().isPresent()
                                          ? this.baseElement.getTargetAnnotation().get().getSimpleName()
                                          : "";

            result.append(ValidationResultDetail.of(
                    targetAnnotationName + " annotate field modifier must be 'final' .",
                    this.baseElement.getElement())
            );
        }

        if (result.isValid() == false) {
            throw new AnnotationProcessorException(result);
        }
    }

    Boolean isValidModifierFinal() {
        return this.baseElement.getElement().getModifiers().stream()
                .anyMatch(modifier -> modifier.equals(Modifier.FINAL));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.baseElement);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FieldElement other = (FieldElement) obj;
        return Objects.equals(this.baseElement, other.baseElement);
    }
}
