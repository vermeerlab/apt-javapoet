/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;

/**
 * 分類が{@link javax.lang.model.element.ElementKind#CLASS} のElementを扱う実装です.
 *
 * @author Yamashita,Takahiro
 */
public class ClassElement implements CategorizedElementInterface {

    final BaseElement baseElement;

    /**
     * For extention
     */
    protected ClassElement() {
        this.baseElement = null;
    }

    protected ClassElement(BaseElement baseElement) {
        this.baseElement = baseElement;
    }

    protected <T extends ClassElement> ClassElement(T classElement) {
        this.baseElement = classElement.baseElement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean canInstance(ElementKind elementKind) {
        return elementKind == ElementKind.CLASS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public <R extends CategorizedElementInterface> Optional<R> create(BaseElement baseElement) {
        return this.canInstance(baseElement.getElement().getKind())
               ? Optional.of((R) new ClassElement(baseElement))
               : Optional.empty();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<Class<? extends Annotation>> getTargetAnnotation() {
        return this.baseElement.getTargetAnnotation();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <A extends Annotation> Optional<A> getAnnotation(Class<A> annotationType) {
        return this.baseElement.getAnnotation(annotationType);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Element getElement() {
        return this.baseElement.getElement();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getPackagePath() {
        return this.baseElement.getPackagePath();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getSimpleName() {
        return this.baseElement.getSimpleName();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R children(Class<? extends Annotation> annotation) {
        return this.baseElement.children(annotation);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R children() {
        return this.baseElement.children();
    }

    /**
     * パッケージパスを編集して返却します.
     *
     * <table summary="ベースパッケージとサブパッケージから生成するクラスのパッケージを編集"  border="thin">
     * <tr>
     * <th>{@literal basePackage}</th>
     * <th>{@literal subPackage}</th>
     * <th>パッケージ編集仕様</th>
     * <th>編集元情報</th>
     * <th>編集後のパッケージパス</th>
     * </tr>
     *
     * <tr>
     * <td>なし</td>
     * <td>なし</td>
     * <td>
     * 注釈した生成元のクラスのパッケージ名にクラス名を小文字にして連結（デフォルト）.<br>
     * デフォルトを同一パッケージにすると既存のクラスとの衝突する可能性があるため、クラス名をサブパッケージにしています.<br>
     * ただし、生成元の資産がデフォルトパッケージの場合は、テストまたは特殊なケースであると考えられるため
     * 生成後のパッケージもデフォルトパッケージにします.
     * <td>
     * 注釈した生成元のクラスパス<br>
     * hoge.piyo.Fuga
     * </td>
     * <td>
     * hoge.piyo.fuga
     * </td>
     * </tr>
     *
     * <tr>
     * <td>あり</td>
     * <td>なし</td>
     * <td>
     * {@literal basePackage} を小文字にした値.<br>
     * 意図して親となるパッケージ名を指定している場合は、<br>
     * 直接指定をしたい強い意図がある場合と考えられるので
     * サブパッケージの追記をしない要件と想定しました.
     * <td>
     * {@code basePackage = "FUGA"}
     * </td>
     * <td>
     * fuga
     * </td>
     * </tr>
     *
     * <tr>
     * <td>なし</td>
     * <td>あり</td>
     * <td>
     * 注釈した生成元のクラスのパッケージ名に{@literal subPackage} を<br>
     * 小文字にして連結.
     * <td>
     * 注釈した生成元のクラスパス<br>
     * hoge.piyo.Fuga<br>
     * <P>
     * {@code subPackage = "SUBFUGA"}
     * </td>
     * <td>
     * hoge.piyo.subfuga
     * </td>
     * </tr>
     *
     * <tr>
     * <td>あり</td>
     * <td>あり</td>
     * <td>
     * {@literal basePackage} と {@literal subPackage} を小文字にして連結.
     * <td>
     * {@code basePackageName = "FUGA", subPackageName = "SUBFUGA"}
     * </td>
     * <td>
     * fuga.subfuga
     * </td>
     * </tr>
     * </table>
     *
     * @param basePackage ベースパッケージ
     * @param subPackage サブパッケージ
     * @return パッケージパス
     */
    public String toPackageName(String basePackage, String subPackage) {
        String _basePackage = basePackage == null ? "" : basePackage;
        String _subPackage = subPackage == null ? "" : subPackage;

        if (_basePackage.equals("") == true && _subPackage.equals("") == true) {
            String packagePath = this.baseElement.getPackagePath();
            String className = this.getSimpleName().toLowerCase(Locale.ENGLISH);
            return packagePath.equals("") ? "" : packagePath + "." + className;
        }

        if (_basePackage.equals("") == false && _subPackage.equals("") == true) {
            return _basePackage.toLowerCase(Locale.ENGLISH);
        }

        if (_basePackage.equals("") == true && _subPackage.equals("") == false) {
            return this.baseElement.getPackagePath() + "." + _subPackage.toLowerCase(
                    Locale.ENGLISH);
        }

        return _basePackage.toLowerCase(Locale.ENGLISH) + "." + _subPackage.toLowerCase(Locale.ENGLISH);
    }

    /**
     * クラス名に接頭語と接尾語を付与した文字列を返却します.
     *
     * @param prifix クラス名に付与したい接頭語
     * @param suffix クラス名に付与した接尾語
     * @return クラス名に接頭語と接尾語を付与した文字列
     */
    public String toClassName(String prifix, String suffix) {
        return prifix + this.getSimpleName() + suffix;
    }

    /**
     * 要素クラスの完全修飾名を返却します.
     *
     * @return 要素の完全修飾名
     */
    public String getQualifiedName() {
        return this.getPackagePath() + "." + this.getSimpleName();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.baseElement);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClassElement other = (ClassElement) obj;
        return Objects.equals(this.baseElement, other.baseElement);
    }

}
