/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.Optional;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.AnnotationProcessorException;

/**
 * {@link javax.lang.model.element.ElementKind} にて分類したElementを扱います.
 *
 * @author Yamashita,Takahiro
 */
public interface CategorizedElementInterface {

    /**
     * 指定種別が分類型インスタンスに適合するか判定します.
     *
     * @param elementKind Elementの種別
     * @return 適合する場合はtrue
     */
    public Boolean canInstance(ElementKind elementKind);

    /**
     * Factoryメソッドである
     * {@link CategorizedElementKind#createCategorizedElement(javax.annotation.processing.ProcessingEnvironment, javax.lang.model.element.Element, java.lang.Class)}
     * を介して 分類済みElementのインスタンスを構築します.
     * <P>
     * 汎用ELementが戻り値の型に変換できない場合は{@code null}を返却します
     *
     * @param <R> 戻り値の型
     * @param baseElement 汎用の分類済みElement
     * @return 構築したインスタンス. 入力値が戻り値の型に変換できない場合は {@code null}
     */
    public <R extends CategorizedElementInterface> Optional<R> create(BaseElement baseElement);

    /**
     * 分類済みElementを作成する条件となったアノテーションを返却します.
     * <P>
     * このアノテーションは分類済みElementをインスタンス化する際に保持したものです.
     *
     * @return 作成条件となったアノテーション.
     */
    public Optional<Class<? extends Annotation>> getTargetAnnotation();

    /**
     * {@link  javax.lang.model.element.Element} に注釈されているアノテーション情報を返却します.
     * <P>
     * Elementに注釈されている作成条件以外の注釈情報を取得したい場合に使用します.
     *
     * @param <A>アノテーションの型
     * @param annotationType 要素から取得したいアノテーションの型
     * @return 指定したアノテーションの情報.
     */
    <A extends Annotation> Optional<A> getAnnotation(Class<A> annotationType);

    /**
     * 分類済みElementの参照元となっている {@link  javax.lang.model.element.Element} を返却します.
     *
     * @return 参照元となっているElement
     */
    public Element getElement();

    /**
     * 要素からパッケージ名を取得して返却します.
     *
     * @return パッケージ名
     */
    public String getPackagePath();

    /**
     * 要素の単純名称を返却します.
     *
     * @return 要素の単純名称
     */
    public String getSimpleName();

    /**
     * 直下の子要素を注釈で抽出して作成した集約リストを返却します.
     * <P>
     * 抽出したすべての要素が同じ型の場合、その型にあった集約クラスを返却します.
     *
     * @param <R> 戻り値の型
     * @param annotation 抽出対象のアノテーション.
     * @return アノテーションで抽出した子要素の分類済みElementの集約クラス
     * @throws AnnotationProcessorException 必須項目が {@code null} の場合
     */
    public <R extends CategorizedElementsInterface> R children(Class<? extends Annotation> annotation);

    /**
     * 直下の子要素の集約クラスを返却します.
     * <P>
     * 抽出したすべての要素が同じ型の場合、その型にあった集約クラスを返却します.
     *
     * @param <R> 戻り値の型
     * @return アノテーションで抽出した子要素の分類済みElementの集約クラス
     * @throws AnnotationProcessorException 必須項目が {@code null} の場合
     */
    public <R extends CategorizedElementsInterface> R children();
}
