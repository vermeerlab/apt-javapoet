/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.ValidationResult;
import org.vermeerlab.apt.ValidationResultDetail;

/**
 * 分類が{@link javax.lang.model.element.ElementKind#METHOD} の分類済みElementリストの集約を扱う実装です.
 *
 * @author Yamashita,Takahiro
 */
public class MethodElements implements CategorizedElementsInterface {

    final BaseElements baseElements;

    /**
     * For extention
     */
    protected MethodElements() {
        this.baseElements = null;
    }

    protected MethodElements(BaseElements baseElements) {
        this.baseElements = baseElements;
    }

    protected <T extends MethodElements> MethodElements(T methodElements) {
        this.baseElements = methodElements.baseElements;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<Class<? extends Annotation>> getTargetAnnotation() {
        return this.baseElements.getTargetAnnotation();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<? extends CategorizedElementsInterface> create(BaseElements baseElements) {
        if (CategorizedElementKind.METHOD.isAllAssignableFrom(baseElements)) {
            return Optional.of(this.toCategorizedElements(baseElements));
        }
        return Optional.empty();

    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked") //ジェネリック
    public <R extends CategorizedElementsInterface> R toCategorizedElements(BaseElements baseElements) {
        return (R) new MethodElements(baseElements);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked") //ジェネリック
    public List<? extends MethodElement> values() {
        return (List<? extends MethodElement>) this.baseElements.values();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Boolean isEmpty() {
        return this.baseElements.isEmpty();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R filter(ElementKind kind) {
        return this.baseElements.filter(kind);
    }

    /**
     * 注釈の対象要素が必須である（１つ以上である）ことを検証します.
     * <P>
     * @return 検証結果.注釈の対象要素が存在しない場合はエラー.
     */
    public ValidationResult validateRequire() {
        return baseElements.validateRequire();
    }

    /**
     * 注釈の対象要素が１つ以下であることを検証します.
     * <P>
     * @return 検証結果.注釈の対象要素が２つ以上である場合はエラー.
     */
    public ValidationResult validateTargetFieldOneOrLess() {
        return baseElements.validateTargetFieldOneOrLess();
    }

    /**
     * 戻り値の型を検証します.
     * <P>
     * Elementの型が等しい、もしくは継承していることを検証します.
     * 検査対象は保持している要素全てです.
     *
     * @param clazz 検証対象の型
     * @return 検証結果.Elementの型が等しい、もしくは継承していない場合、エラー
     */
    public ValidationResult validateReturnTypeIsSame(Class<?> clazz) {
        List<ValidationResultDetail> details = this.values().stream()
                .map(methodElement -> {
                    return methodElement.validateReturnTypeIsSame(clazz);
                })
                .map(ValidationResult::getAllDetails)
                .flatMap(List::stream)
                .collect(Collectors.toList());
        return ValidationResult.of(details);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.baseElements);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MethodElements other = (MethodElements) obj;
        return Objects.equals(this.baseElements, other.baseElements);
    }
}
