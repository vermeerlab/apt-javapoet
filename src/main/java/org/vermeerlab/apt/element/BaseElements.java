/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.ValidationResult;
import org.vermeerlab.apt.ValidationResultDetail;

/**
 * 汎用的な分類済みElementリストの集約を扱います.
 *
 * @author Yamashita,Takahiro
 */
public class BaseElements implements CategorizedElementsInterface {

    final List<? extends CategorizedElementInterface> categorizedElementList;
    final Class<? extends Annotation> targetAnnotaion;

    /**
     * For extention
     */
    protected BaseElements() {
        this(Collections.emptyList(), null);
    }

    protected BaseElements(List<? extends CategorizedElementInterface> categorizedElement, Class<? extends Annotation> annotation) {
        this.categorizedElementList = Collections.unmodifiableList(categorizedElement);
        this.targetAnnotaion = annotation;
    }

    /**
     * インスタンスを構築します.
     *
     * @param categorizedElement 分類Element
     * @param annotation リスト作成の根拠となったアノテーション.{@code null}の場合は、全要素を取得対象とします.
     * @return 子要素リスト.
     */
    protected static BaseElements of(List<? extends CategorizedElementInterface> categorizedElement, Class<? extends Annotation> annotation) {
        return new BaseElements(categorizedElement, annotation);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Class<? extends Annotation>> getTargetAnnotation() {
        return Optional.ofNullable(this.targetAnnotaion);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<? extends CategorizedElementsInterface> create(BaseElements baseElements) {
        return Optional.of(baseElements);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked") //ジェネリック
    public <R extends CategorizedElementsInterface> R toCategorizedElements(BaseElements baseElements) {
        return (R) baseElements;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<? extends CategorizedElementInterface> values() {
        return this.categorizedElementList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean isEmpty() {
        return this.categorizedElementList.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <R extends CategorizedElementsInterface> R filter(ElementKind kind) {
        List<? extends CategorizedElementInterface> kindElements = this.categorizedElementList.stream()
                .filter(value -> value.getElement().getKind() == kind)
                .collect(Collectors.toList());

        return CategorizedElementKind.createCategorizedElementsByKind(kindElements, this.targetAnnotaion, kind);
    }

    /**
     * 注釈の対象要素が必須である（１つ以上である）ことを検証します.
     * <P>
     * @return 検証結果.注釈の対象要素が存在しない場合はエラー.
     */
    public ValidationResult validateRequire() {
        ValidationResult result = ValidationResult.create();
        if (this.categorizedElementList.isEmpty()) {
            String message = this.getTargetAnnotation().isPresent()
                             ? "Target item is required annotated item by " + this.getTargetAnnotation().get().getSimpleName() + " ."
                             : "Item require.";
            return result.append(ValidationResultDetail.of(message));
        }
        return result;
    }

    /**
     * 注釈の対象要素が１つ以下であることを検証します.
     * <P>
     * @return 検証結果.注釈の対象要素が２つ以上である場合はエラー.
     */
    public ValidationResult validateTargetFieldOneOrLess() {
        ValidationResult result = ValidationResult.create();
        if (1 < this.categorizedElementList.size()) {
            String message = this.getTargetAnnotation().isPresent()
                             ? this.getTargetAnnotation().get().getSimpleName() + " can not annotate multiple items."
                             : "item count must be one or less.";
            return result.append(ValidationResultDetail.of(message));
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.categorizedElementList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaseElements other = (BaseElements) obj;
        return Objects.equals(this.categorizedElementList, other.categorizedElementList);
    }
}
