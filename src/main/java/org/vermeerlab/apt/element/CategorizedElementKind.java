/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy create the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Optional;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.javapoet.JavaPoetException;

/**
 * {@link javax.lang.model.element.ElementKind} で分類したElementを扱う機能を提供します.
 *
 * <table summary="実装済みの分類と実装"  border="thin">
 * <tr>
 * <th>{@link javax.lang.model.element.ElementKind}</th>
 * <th>実装クラス</th>
 * <th>実装クラス（集約）</th>
 * </tr>
 *
 * <tr>
 * <td>{@code PACKAGE}</td>
 * <td>{@link org.vermeerlab.apt.element.PackageElement}</td>
 * <td>Non</td>
 * </tr>
 * <tr>
 * <td>{@code CLASS}</td>
 * <td>{@link org.vermeerlab.apt.element.ClassElement}</td>
 * <td>{@link org.vermeerlab.apt.element.ClassElements}</td>
 * </tr>
 * <tr>
 * <td>{@code METHOD}</td>
 * <td>{@link org.vermeerlab.apt.element.MethodElement}</td>
 * <td>{@link org.vermeerlab.apt.element.MethodElements}</td>
 * </tr>
 * <tr>
 * <td>{@code FIELD}</td>
 * <td>{@link org.vermeerlab.apt.element.FieldElement}</td>
 * <td>{@link org.vermeerlab.apt.element.FieldElements}</td>
 * </tr>
 * <tr>
 * <td>{@code OTHER}</td>
 * <td>{@link org.vermeerlab.apt.element.BaseElement}</td>
 * <td>{@link org.vermeerlab.apt.element.BaseElements}</td>
 * </tr>
 * </table>
 *
 * @author Yamashita,Takahiro
 */
public enum CategorizedElementKind {
    PACKAGE(new PackageElement(), null),
    CLASS(new ClassElement(), new ClassElements()),
    METHOD(new MethodElement(), new MethodElements()),
    FIELD(new FieldElement(), new FieldElements()),
    OTHER(new BaseElement(), new BaseElements());

    private final CategorizedElementInterface categorizedElement;
    private final CategorizedElementsInterface categorizedElements;

    private CategorizedElementKind(CategorizedElementInterface categorizedElement, CategorizedElementsInterface categorizedElements) {
        this.categorizedElement = categorizedElement;
        this.categorizedElements = categorizedElements;
    }

    /**
     * 分類済み要素のインスタンスを構築します.
     * <P>
     * 処理対象の要素の分類（{@link javax.lang.model.element.ElementKind}）に応じた分類済み要素の実装がない場合は、
     * 型を特定できないため、汎用的な {@link org.vermeerlab.apt.element.BaseElement}
     * でインスタンス化します.
     *
     * @param <R> 戻り値の型
     * @param processingEnv {@literal AnnotationProcessor}
     * 実行時に取得した{@link javax.annotation.processing.ProcessingEnvironment}
     * @param element {@literal AnnotationProcessor} で取得した要素
     * @param anntation Elementが対象としたアノテーション.
     * {@code null} を指定した場合は、分類済みElementの対象とするアノテーションが未設定（{@code null}）になります.
     * @return 分類Elementインスタンス.分類Elementの実装をしていない場合は、汎用的な{@link BaseElement} を適用します.
     * @throws JavaPoetException processingEnv または element （もしくは両方）が {@code null}の場合
     */
    @SuppressWarnings("unchecked")
    public static <R extends CategorizedElementInterface> R createCategorizedElement(ProcessingEnvironment processingEnv, Element element, Class<? extends Annotation> anntation) {
        if (processingEnv == null || element == null) {
            throw new JavaPoetException("processingEnv or element is null. processingEnv and element must set.");
        }

        BaseElement baseElement = BaseElement.of(processingEnv, element, anntation);

        Optional<? extends CategorizedElementInterface> categorizedElement = Optional.empty();

        for (CategorizedElementKind kind : CategorizedElementKind.values()) {
            categorizedElement = kind.categorizedElement.create(baseElement);
            if (categorizedElement.isPresent()) {
                break;
            }
        }

        return (R) categorizedElement.get();
    }

    /**
     * 分類済み要素リストの集約インスタンスを構築します.
     * <P>
     * 処理対象の要素の分類（{@link javax.lang.model.element.ElementKind}）に応じた分類済み要素の実装がない場合や
     * 要素リストの分類種別が１種類でない場合は 型を特定できないため、汎用的な {@link org.vermeerlab.apt.element.BaseElements}
     * でインスタンス化します.
     *
     * @param <R> 戻り値の分類済みElementの集約の型
     * @param categorizedElements 分類済みElementのリスト.
     * @param annotation リスト作成の根拠となったアノテーション
     * @return 同一分類で集約したElementリストの集約クラス
     * @throws JavaPoetException categorizedElementsが {@code null}の場合
     */
    @SuppressWarnings("unchecked") //キャスト未検査の警告対応
    public static <R extends CategorizedElementsInterface> R createCategorizedElements(
            List<? extends CategorizedElementInterface> categorizedElements, Class<? extends Annotation> annotation) {
        if (categorizedElements == null) {
            throw new JavaPoetException("categorizedElements is null. categorizedElements must set.");
        }

        BaseElements baseElements = BaseElements.of(categorizedElements, annotation);

        Optional<? extends CategorizedElementsInterface> _categorizedElements = Optional.empty();

        for (CategorizedElementKind kind : CategorizedElementKind.values()) {
            if (kind.categorizedElements == null) {
                continue;
            }
            _categorizedElements = kind.categorizedElements.create(baseElements);
            if (_categorizedElements.isPresent()) {
                break;
            }
        }

        return (R) _categorizedElements.get();
    }

    /**
     * 指定した種別（{@link javax.lang.model.element.ElementKind}）に応じた分類済み要素リストの集約インスタンスを構築します.
     * <P>
     * 対象が存在しない場合も 戻り値の型は、指定した種別に適合する型の集約クラスで返却します.
     *
     * @param <R> 戻り値の分類済みElementの集約の型
     * @param categorizedElements 分類済みElementのリスト.
     * @param annotation リスト作成の根拠となったアノテーション
     * @param elementKind 抽出条件に指定した要素の型
     * @return 同一種別で集約したElementリストの集約クラス
     * @throws JavaPoetException categorizedElementsが {@code null}の場合
     */
    @SuppressWarnings("unchecked") //キャスト未検査の警告対応
    public static <R extends CategorizedElementsInterface> R createCategorizedElementsByKind(
            List<? extends CategorizedElementInterface> categorizedElements, Class<? extends Annotation> annotation, ElementKind elementKind) {

        R instances = CategorizedElementKind.createCategorizedElements(categorizedElements, annotation);

        // 要素が存在する＝対象の型があった
        if (instances.isEmpty() == false) {
            return instances;
        }

        for (CategorizedElementKind kind : CategorizedElementKind.values()) {
            if (kind.categorizedElement.canInstance(elementKind)) {
                return (R) kind.categorizedElements.toCategorizedElements((BaseElements) instances);
            }
        }

        return instances;
    }

    /**
     * インスタンスが分類と等しいか判定します.
     *
     * @param categorizedElement 検証対象の分類済みElement
     * @return 分類と等しい場合は true
     */
    public boolean isAssignableFrom(CategorizedElementInterface categorizedElement) {
        return this.categorizedElement.getClass().isAssignableFrom(categorizedElement.getClass());
    }

    /**
     * 集約内全てのインスタンスの分類が一致しているか判定します.
     *
     * @param categorizedElements 検証対象の分類済みElementの集約クラス
     * @return 分類と一致している場合は true, 不一致および要素が無い場合は false
     */
    public boolean isAllAssignableFrom(CategorizedElementsInterface categorizedElements) {
        if (categorizedElements.isEmpty()) {
            return false;
        }
        return categorizedElements.values().stream()
                .allMatch(this::isAssignableFrom);
    }
}
