/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.Objects;
import java.util.Optional;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;

/**
 * 分類が{@link javax.lang.model.element.ElementKind#PACKAGE} のElementを扱う実装です.
 *
 * @author Yamashita,Takahiro
 */
public class PackageElement implements CategorizedElementInterface {

    final BaseElement baseElement;

    /**
     * For extention
     */
    protected PackageElement() {
        this.baseElement = null;
    }

    protected PackageElement(BaseElement baseElement) {
        this.baseElement = baseElement;
    }

    protected <T extends PackageElement> PackageElement(T packageElement) {
        this.baseElement = packageElement.baseElement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean canInstance(ElementKind elementKind) {
        return elementKind == ElementKind.PACKAGE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public <R extends CategorizedElementInterface> Optional<R> create(BaseElement baseElement) {
        return this.canInstance(baseElement.getElement().getKind())
               ? Optional.of((R) new PackageElement(baseElement))
               : Optional.empty();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<Class<? extends Annotation>> getTargetAnnotation() {
        return this.baseElement.getTargetAnnotation();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <A extends Annotation> Optional<A> getAnnotation(Class<A> annotationType) {
        return this.baseElement.getAnnotation(annotationType);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Element getElement() {
        return this.baseElement.getElement();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getPackagePath() {
        return this.baseElement.getPackagePath();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getSimpleName() {
        return this.baseElement.getSimpleName();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R children(Class<? extends Annotation> annotation) {
        return this.baseElement.children(annotation);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R children() {
        return this.baseElement.children();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.baseElement);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PackageElement other = (PackageElement) obj;
        return Objects.equals(this.baseElement, other.baseElement);
    }
}
