/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy create the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.AnnotationProcessorException;
import org.vermeerlab.apt.ProcessingEnvironmentUtil;

/**
 * {@link javax.lang.model.element.ElementKind} で分類したElementを扱う基本となるクラスです.
 * <P>
 * {@link javax.lang.model.element.ElementKind} により分類するElementクラスが存在しない場合、
 * 汎用的な分類済みElementとしても使用します.
 *
 * @author Yamashita,Takahiro
 */
public class BaseElement implements CategorizedElementInterface {

    final ProcessingEnvironmentUtil processingEnvHelper;
    final ProcessingEnvironment processingEnv;
    final Element element;
    final Class<? extends Annotation> targetAnnotation;

    /**
     * For extention
     */
    protected BaseElement() {
        this(null, null, null, null);
    }

    protected BaseElement(ProcessingEnvironmentUtil processingEnvironmentUtil, ProcessingEnvironment processingEnv, Element element, Class<? extends Annotation> annotation) {
        this.processingEnvHelper = processingEnvironmentUtil;
        this.processingEnv = processingEnv;
        this.element = element;
        this.targetAnnotation = annotation;
    }

    protected BaseElement(BaseElement baseElement) {
        this(baseElement.processingEnvHelper, baseElement.processingEnv, baseElement.element,
             baseElement.targetAnnotation);
    }

    /**
     * インスタンスを構築します.
     *
     * @param processingEnv AnnotaionProcessor実行時の環境情報
     * @param element 作成対象のルートとなるElement
     * @param annotation 分類済みElementの作成対象となったアノテーション
     * @return 構築したインスタンス
     */
    protected static BaseElement of(
            ProcessingEnvironment processingEnv, Element element, Class<? extends Annotation> annotation) {
        ProcessingEnvironmentUtil processingEnvironmentUtil = ProcessingEnvironmentUtil.of(processingEnv);

        return new BaseElement(processingEnvironmentUtil, processingEnv, element, annotation);
    }

    /**
     * {@inheritDoc}
     * <P>
     * 汎用型＝全てに適合しないため falseを固定で返却します.
     */
    @Override
    public Boolean canInstance(ElementKind elementKind) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public <R extends CategorizedElementInterface> Optional<R> create(BaseElement baseElement) {
        return Optional.of((R) baseElement);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<Class<? extends Annotation>> getTargetAnnotation() {
        return Optional.ofNullable(this.targetAnnotation);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <A extends Annotation> Optional<A> getAnnotation(Class<A> annotationType) {
        return Optional.ofNullable(this.element.getAnnotation(annotationType));
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Element getElement() {
        return this.element;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getPackagePath() {
        return this.processingEnvHelper.getPackagePath(this.element);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String getSimpleName() {
        return this.element.getSimpleName().toString();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked") //キャスト未検査の警告対応
    public <R extends CategorizedElementsInterface> R children(Class<? extends Annotation> annotation) {
        this.validateChildren(annotation);

        List<CategorizedElementInterface> categorizedElements = this.element.getEnclosedElements().stream()
                .filter(_element -> _element.getAnnotation(annotation) != null)
                .map(_element -> CategorizedElementKind.createCategorizedElement(this.processingEnv, _element,
                                                                                 annotation))
                .map(CategorizedElementInterface.class::cast)
                .collect(Collectors.toList());

        return CategorizedElementKind.createCategorizedElements(categorizedElements, annotation);
    }

    void validateChildren(Class<? extends Annotation> annotation) {
        if (annotation == null) {
            throw new AnnotationProcessorException("annotation is required for getting child elements .");
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked") //キャスト未検査の警告対応
    public <R extends CategorizedElementsInterface> R children() {
        List<? extends CategorizedElementInterface> categorizedElements = this.element.getEnclosedElements().stream()
                .filter(this::canInstanceChilldElememt)
                .map(_element -> CategorizedElementKind.createCategorizedElement(this.processingEnv, _element, null))
                .map(CategorizedElementInterface.class::cast)
                .collect(Collectors.toList());

        return CategorizedElementKind.createCategorizedElements(categorizedElements, null);
    }

    /**
     * 子要素の取得対象判定です.
     * <P>
     * 子要素の取得でデフォルトコンストラクタを除外することを意図した判定を行います.
     *
     * @param element 検査対象の要素
     * @return 対象外の場合はfalse
     */
    Boolean canInstanceChilldElememt(Element element) {
        return (element.getAnnotationMirrors().isEmpty()
                && element.getKind().equals(ElementKind.CONSTRUCTOR)
                && element.getEnclosedElements().isEmpty()) == false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.element);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaseElement other = (BaseElement) obj;
        return Objects.equals(this.element, other.element);
    }

}
