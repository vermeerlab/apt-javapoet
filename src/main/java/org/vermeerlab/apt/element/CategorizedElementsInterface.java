/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Optional;
import javax.lang.model.element.ElementKind;

/**
 * {@link javax.lang.model.element.ElementKind} にて分類した複数の分類済みElementの集約を扱います.
 *
 * @author Yamashita,Takahiro
 */
public interface CategorizedElementsInterface {

    /**
     * 子要素を抽出する根拠となった分類済みElementをアノテーションを返却します.
     * <P>
     * 抽出時にアノテーションを未指定（すべての子要素を取得する）の場合、{@code null}を返却します.
     *
     * @return 作成条件となったアノテーション
     */
    public Optional<Class<? extends Annotation>> getTargetAnnotation();

    /**
     * Factoryメソッドである{@link CategorizedElementKind#createCategorizedElements(java.util.List, java.lang.Class) } を介して
     * 分類済みElementのインスタンスを構築します.
     * <P>
     * 全ての分類が同じ場合かつ、分類済みElementリストの集約クラスがある場合は 適合する型の集約クラスに変換して返却します.<br>
     * 分類と一致しない または複数の分類が混在している場合 {@code null} を返却します.
     *
     * @param baseElements 汎用分類Elementリストの集約クラス
     * @return 構築したインスタンス.
     */
    @SuppressWarnings("unchecked") //ジェネリック
    public Optional<? extends CategorizedElementsInterface> create(BaseElements baseElements);

    /**
     * 分類済みELementに汎用分類Elementを変換したインスタンスを返却します.
     *
     * @param <R> 戻り値の型
     * @param baseElements 汎用分類Elementリストの集約クラス
     * @return 構築したインスタンス.
     */
    @SuppressWarnings("unchecked") //ジェネリック
    public <R extends CategorizedElementsInterface> R toCategorizedElements(BaseElements baseElements);

    /**
     * 分類済みElementのリストを返却します.
     *
     * @return 分類済みElementのリスト
     */
    List<? extends CategorizedElementInterface> values();

    /**
     * 保持する要素が０件であるか判定します.
     *
     * @return 要素数が０件の場合、true
     */
    public Boolean isEmpty();

    /**
     * 要素種別に合致する分類済みElementを抽出して返却します.
     * <P>
     * リスト作成の根拠となったアノテーションは抽出元から引き継ぎます.
     * 未指定（{@code null}）の場合も そのまま引き継ぎます.
     *
     * @param <R> 戻り値の型
     * @param kind 要素種別
     * @return 要素種別が合致した分類済みElementの集約
     */
    @SuppressWarnings("unchecked") //ジェネリック
    public <R extends CategorizedElementsInterface> R filter(ElementKind kind);

}
