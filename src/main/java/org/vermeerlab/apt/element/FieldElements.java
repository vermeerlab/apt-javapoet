/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.ValidationResult;
import org.vermeerlab.apt.ValidationResultDetail;

/**
 * 分類が{@link javax.lang.model.element.ElementKind#FIELD} の分類済みElementリストの集約を扱う実装です.
 *
 * @author Yamashita,Takahiro
 */
public class FieldElements implements CategorizedElementsInterface {

    final BaseElements baseElements;

    /**
     * For extention
     */
    protected FieldElements() {
        this.baseElements = null;
    }

    protected FieldElements(BaseElements baseElements) {
        this.baseElements = baseElements;
    }

    protected <T extends FieldElements> FieldElements(T fieldElements) {
        this.baseElements = fieldElements.baseElements;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<Class<? extends Annotation>> getTargetAnnotation() {
        return baseElements.getTargetAnnotation();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Optional<? extends CategorizedElementsInterface> create(BaseElements baseElements) {
        if (CategorizedElementKind.FIELD.isAllAssignableFrom(baseElements)) {
            return Optional.of(this.toCategorizedElements(baseElements));
        }
        return Optional.empty();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked") //ジェネリック
    public <R extends CategorizedElementsInterface> R toCategorizedElements(BaseElements baseElements) {
        return (R) new FieldElements(baseElements);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked") //ジェネリック
    public List<? extends FieldElement> values() {
        return (List<? extends FieldElement>) this.baseElements.values();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Boolean isEmpty() {
        return this.baseElements.isEmpty();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public <R extends CategorizedElementsInterface> R filter(ElementKind kind) {
        return this.baseElements.filter(kind);
    }

    /**
     * 値が一致する要素のみを抽出したインスタンスを新たに構築します.
     *
     * @param value 抽出する値
     * @return 値が一致する要素のみを抽出した新たなインスタンス
     */
    public FieldElements filterConstantValue(String value) {
        List<FieldElement> items = this.values().stream()
                .filter(f -> f.getConstantValue().isPresent())
                .filter(f -> f.getConstantValue().get().equals(value))
                .collect(Collectors.toList());
        return this.createInstance(items);
    }

    /**
     * 値が一致する要素のみを抽出したインスタンスを新たに構築します.
     * <P>
     * 抽出する値の型を {@link  java.util.Set} にすることで、抽出条件自体に重複が無い事を保証しています.
     *
     * @param values 抽出する値のセット.
     * @return 値が一致する要素のみを抽出した新たなインスタンス
     */
    public FieldElements filterConstantValue(Set<String> values) {
        List<FieldElement> items = this.values().stream()
                .filter(f -> f.getConstantValue().isPresent())
                .filter(f -> values.contains(f.getConstantValue().get()))
                .collect(Collectors.toList());
        return this.createInstance(items);
    }

    /**
     * リストから集約クラスのインスタンスを構築します.
     * <P>
     * リスト作成の根拠となったアノテーション情報を引き継ぎます.
     *
     * @param fieldElements インスタンス複製元のリスト
     * @return
     */
    FieldElements createInstance(List<FieldElement> fieldElements) {
        Class<? extends Annotation> annotation = this.getTargetAnnotation().orElse(null);
        BaseElements _categorizedElements = BaseElements.of(fieldElements, annotation);
        return new FieldElements(_categorizedElements);
    }

    /**
     * フィールド値の重複を検証します.
     *
     * @return 検証結果.検証対象にフィールド値の重複があった場合はエラー
     */
    public ValidationResult validateDuplicate() {
        Map<String, Long> countMap = this.values().stream()
                .filter(fieldElement -> fieldElement.getConstantValue().isPresent())
                .map(fieldElement -> fieldElement.getConstantValue().get())
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));

        if (this.values().size() == countMap.size()) {
            return ValidationResult.create();
        }

        Set<String> duplicateValues = countMap.entrySet().stream()
                .filter(e -> 1 < e.getValue())
                .map(e -> e.getKey())
                .collect(Collectors.toSet());

        List<ValidationResultDetail> details = this.filterConstantValue(duplicateValues).values().stream()
                .map(e -> {
                    String targetAnnotationName = this.getTargetAnnotation().isPresent()
                                                  ? this.getTargetAnnotation().get().getSimpleName()
                                                  : "";

                    return ValidationResultDetail.of(
                            targetAnnotationName + " annotated fields are duplicate value.",
                            e.getElement());
                })
                .collect(Collectors.toList());

        return ValidationResult.of(details);
    }

    /**
     * 注釈の対象要素が必須である（１つ以上である）ことを検証します.
     * <P>
     * @return 検証結果.注釈の対象要素が存在しない場合はエラー.
     */
    public ValidationResult validateRequire() {
        return this.baseElements.validateRequire();
    }

    /**
     * 注釈の対象要素が１つ以下であることを検証します.
     * <P>
     * @return 検証結果.注釈の対象要素が２つ以上である場合はエラー.
     */
    public ValidationResult validateTargetFieldOneOrLess() {
        return this.baseElements.validateTargetFieldOneOrLess();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.baseElements);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FieldElements other = (FieldElements) obj;
        return Objects.equals(this.baseElements, other.baseElements);
    }

}
