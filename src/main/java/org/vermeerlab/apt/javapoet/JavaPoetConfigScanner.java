/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.javapoet;

import java.util.Iterator;
import java.util.List;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.apt.config.CommandConfigScannerInterface;
import org.vermeerlab.apt.config.ConfigXmlReader;

/**
 * 定義用Xmlから取得した定義情報を扱います.
 * <P>
 * {@code <javaPoetCommand>} の {@code value未指定}を共通定義として、
 * {@code value} に指定したAnnotationProcessorCommand 毎の個別定義を適用します.<br>
 * 個別定義は共通定義の差分として編集を行います.
 *
 * @author Yamashita,Takahiro
 */
public class JavaPoetConfigScanner implements CommandConfigScannerInterface {

    JavaPoetConfig priorityJavaPoetConfig;
    JavaPoetConfigMap javaPoetConfigMap;

    public JavaPoetConfigScanner() {
        this.javaPoetConfigMap = new JavaPoetConfigMap();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public CommandConfigScannerInterface readConfig(ConfigXmlReader configXmlReader) {
        this.scanPriorityJavaPoetConfig(configXmlReader);
        this.scanExtentionJavaPoetConfig(configXmlReader);
        return this;
    }

    //
    void scanPriorityJavaPoetConfig(ConfigXmlReader configXmlReader) {
        JavaPoetConfig javaPoetConfig = this.editJavaPoetConfig(configXmlReader, "not(@value)");
        this.priorityJavaPoetConfig = javaPoetConfig;
    }

    //
    void scanExtentionJavaPoetConfig(ConfigXmlReader configXmlReader) {
        List<String> attrs = configXmlReader.scanTextValue("//javaPoetCommand[@value]/@value");

        JavaPoetConfigMap _javaPoetConfigMap = new JavaPoetConfigMap();
        for (String attr : attrs) {
            String xpath = "@value=" + "'" + attr + "'";
            JavaPoetConfig javaPoetConfig = this.editJavaPoetConfig(configXmlReader, xpath);
            _javaPoetConfigMap.put(attr, javaPoetConfig);
        }

        this.javaPoetConfigMap = _javaPoetConfigMap;
    }

    //
    JavaPoetConfig editJavaPoetConfig(ConfigXmlReader configXmlReader, String xpathCondition) {
        Iterator<String> licenseTemplateList
                         = configXmlReader.scanTextValue(
                        "//javaPoetCommand[" + xpathCondition + "]/licenseTemplate/text()").iterator();
        String licenseTemplate = licenseTemplateList.hasNext()
                                 ? licenseTemplateList.next()
                                 : "";

        Iterator<String> copyRightYearList = configXmlReader.scanTextValue(
                "//javaPoetCommand[" + xpathCondition + "]/copyRightYear/text()").iterator();
        String copyRightYear = copyRightYearList.hasNext()
                               ? copyRightYearList.next()
                               : "";

        Iterator<String> ownerNameList = configXmlReader.scanTextValue(
                "//javaPoetCommand[" + xpathCondition + "]/ownerName/text()").iterator();
        String ownerName = ownerNameList.hasNext()
                           ? ownerNameList.next()
                           : "";

        Iterator<String> classJavaDocList = configXmlReader.scanTextValue(
                "//javaPoetCommand[" + xpathCondition + "]/classJavaDoc/text()").iterator();
        String classJavaDoc = classJavaDocList.hasNext()
                              ? classJavaDocList.next()
                              : "";

        List<String> sinces = configXmlReader.scanTextValue(
                "//javaPoetCommand[" + xpathCondition + "]/sinces/since/text()");

        List<String> versions = configXmlReader.scanTextValue(
                "//javaPoetCommand[" + xpathCondition + "]/versions/version/text()");

        List<String> authors = configXmlReader.scanTextValue(
                "//javaPoetCommand[" + xpathCondition + "]/authors/author/text()");

        return new JavaPoetConfig(licenseTemplate, copyRightYear, ownerName, classJavaDoc, sinces, versions, authors);
    }

    /**
     * 実行コマンドに使用する定義用Xmlの情報を取得して返却します.
     *
     * @param <T> 実行コマンドの型
     * @param command 実行コマンドのインスタンス
     * @return 実行コマンドに使用する定義情報
     */
    public <T extends ProcessorCommandInterface> JavaPoetConfig getJavaPoetConfig(T command) {
        JavaPoetConfig extentionJavaPoetConfig
                       = this.javaPoetConfigMap.get(command.getClass().getCanonicalName()).orElse(priorityJavaPoetConfig);

        JavaPoetConfig mergedJavaPoetConfig = priorityJavaPoetConfig.mergeByCommand(extentionJavaPoetConfig, command);
        return mergedJavaPoetConfig;
    }
}
