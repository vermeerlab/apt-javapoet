/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
/**
 * {@literal JavaPoet}を使用してコード生成および要素検査をする機能を提供します.
 * <P>
 * {@link AbstractJavaPoetProcessorCommand}を継承することで
 * 本パッケージを使用した{@literal Annotation Processor}の実行コマンドとして扱われます.
 */
package org.vermeerlab.apt.javapoet;
