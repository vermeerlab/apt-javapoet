/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.javapoet;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import org.vermeerlab.apt.config.CommandConfigScannerInterface;

/**
 * {@link AbstractJavaPoetProcessorCommand} にてCommandConfigScannerを一時退避するクラスです.
 *
 * @author Yamashita,Takahiro
 */
class CommandConfigScannerMap {

    final private Map<Class<? extends CommandConfigScannerInterface>, CommandConfigScannerInterface> map;

    CommandConfigScannerMap() {
        map = new ConcurrentHashMap<>();
    }

    <T extends CommandConfigScannerInterface> void put(List<T> commandConfigScanner) {
        commandConfigScanner.stream()
                .forEachOrdered(scanner -> {
                    this.map.put(scanner.getClass(), scanner);
                });
    }

    @SuppressWarnings("unchecked") // generic cast
    <T extends CommandConfigScannerInterface> Optional<T> get(Class<T> commandConfigScanner) {
        return Optional.ofNullable((T) this.map.get(commandConfigScanner));
    }
}
