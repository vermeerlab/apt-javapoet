/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.javapoet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.apt.config.CommandConfigScannerInterface;

/**
 * {@literal JavaPoet}を使用してコード生成および要素検査をする{@literal Processor Command}の基底クラスです.
 *
 * @author Yamashita,Takahiro
 */
public abstract class AbstractJavaPoetProcessorCommand implements ProcessorCommandInterface {

    CommandConfigScannerMap commandConfigScannerMap;

    public AbstractJavaPoetProcessorCommand() {
        this.commandConfigScannerMap = new CommandConfigScannerMap();
    }

    /**
     * {@inheritDoc }
     * <P>
     * 必須の{@link JavaPoetConfigScanner}と
     * 拡張クラス側で指定する任意のコマンド定義情報
     * （{@link AbstractJavaPoetProcessorCommand#getExtentionCommandConfigScannerClasses()}）のリストを返却します.
     */
    @Override
    final public List<Class<? extends CommandConfigScannerInterface>> getCommandConfigScannerClasses() {
        List<Class<? extends CommandConfigScannerInterface>> classes = new ArrayList<>();
        classes.add(JavaPoetConfigScanner.class);
        classes.addAll(this.getExtentionCommandConfigScannerClasses());
        return Collections.unmodifiableList(classes);
    }

    /**
     * コマンド実行にあたって定義用Xmlから独自に情報を取得する必要がある場合に実装してください.
     *
     * @return コマンド定義取得クラス
     */
    protected List<Class<? extends CommandConfigScannerInterface>> getExtentionCommandConfigScannerClasses() {
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    final public <T extends CommandConfigScannerInterface> ProcessorCommandInterface setCommandConfigScanners(List< T> commandConfigScanners) {
        this.commandConfigScannerMap.put(commandConfigScanners);
        return this;
    }

    /**
     * コマンドクラスで使用する定義用Xmlの情報を取得したインスタンスを返却します.
     *
     * @param <T> パラメータのクラスおよび戻り値の型
     * @param commandConfigScannerClass コマンドクラスで使用する定義用Xmlの情報を取得するクラス
     * @return コマンドクラスで使用する定義用Xml情報を取得したインスタンス
     */
    public <T extends CommandConfigScannerInterface> Optional<T> getCommandConfigScanner(Class<T> commandConfigScannerClass) {
        return this.commandConfigScannerMap.get(commandConfigScannerClass);
    }

    /**
     * JavaPoetによるコード生成および要素検証時に使用する定義用Xmlの情報を操作するクラスを返却します.
     *
     * @return JavaPoetによるコード生成および要素検証時に使用する定義用Xmlの情報を操作するクラス
     */
    public JavaPoetConfig getJavaPoetConfig() {
        JavaPoetConfigScanner javaPoetConfigScanner = this.getCommandConfigScanner(JavaPoetConfigScanner.class).get();
        return javaPoetConfigScanner.getJavaPoetConfig(this);
    }
}
