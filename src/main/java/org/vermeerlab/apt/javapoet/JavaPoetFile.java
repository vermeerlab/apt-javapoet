/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy setCode the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.javapoet;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;
import java.io.IOException;
import java.util.Collections;
import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;

/**
 * {@link com.squareup.javapoet.JavaFile} をラッパーした機能です.
 * <P>
 * 出力するクラスコードに共通した編集を行います.
 * <ul>
 * <li>クラスコメントに共通して記述</li>
 * <li>生成アノテーションの付与</li>
 * </ul>
 *
 */
public class JavaPoetFile {

    // Space 4
    private static final String INDENT = "    ";

    JavaPoetConfig javaPoetConfig;
    JavaFile javaFile;

    /**
     * for expansion
     */
    protected JavaPoetFile() {
    }

    protected JavaPoetFile(JavaPoetConfig javaPoetConfig) {
        this.javaPoetConfig = javaPoetConfig;
    }

    /**
     * 実行コマンドに応じた定義情報を取得してクラスコメントを編集したインスタンスを構築します.
     *
     * @param javaPoetProcessorCommand JavaPoetを使用してコードを生成する実行コマンドクラス
     * @param classJavaDocTitle 追記するJavaDocのTitleコメント.
     * @param replaceChars クラスコメント内にある {@literal "$S"} を置換するための文字列（指定純に適用）
     *
     * @return 構築したインスタンス
     */
    public static JavaPoetFile of(AbstractJavaPoetProcessorCommand javaPoetProcessorCommand, String classJavaDocTitle, String... replaceChars) {
        JavaPoetConfig javaPoetConfig = javaPoetProcessorCommand.getJavaPoetConfig();
        JavaPoetConfig mergedJavaPoetConfig = javaPoetConfig.mergeClassJavaDocComment(classJavaDocTitle, replaceChars);
        return new JavaPoetFile(mergedJavaPoetConfig);
    }

    /**
     * 実行コマンドに応じた定義情報を取得したインスタンスを構築します.
     *
     * @param javaPoetProcessorCommand JavaPoetを使用してコードを生成する実行コマンドクラス
     * @return 構築したインスタンス
     */
    public static JavaPoetFile of(AbstractJavaPoetProcessorCommand javaPoetProcessorCommand) {
        JavaPoetConfig javaPoetConfig = javaPoetProcessorCommand.getJavaPoetConfig();
        return new JavaPoetFile(javaPoetConfig);
    }

    /**
     * 定義情報を使用しないインスタンスを構築します.
     *
     * @return 構築したインスタンス
     */
    public static JavaPoetFile of() {
        JavaPoetConfig javaPoetConfig = new JavaPoetConfig("", "", "", "", Collections.emptyList(),
                                                           Collections.emptyList(), Collections.emptyList());;
        return new JavaPoetFile(javaPoetConfig);
    }

    /**
     * 生成するJavaコードを保持したインスタンスを返却します.
     *
     * @param packageName 出力クラスのパッケージパス
     * @param typeSpecBuilder 出力するJavaFileのコードを編集した情報
     * @return JavaCodeを保持したインスタンス
     */
    public JavaPoetFile setCode(String packageName, TypeSpec.Builder typeSpecBuilder) {
        typeSpecBuilder.addAnnotation(this.javaPoetConfig.classGeneratedAnnotationSpec()
        );

        typeSpecBuilder.addJavadoc(this.javaPoetConfig.classComment());

        String _packageName = packageName == null ? "" : packageName;

        this.javaFile = JavaFile.builder(_packageName, typeSpecBuilder.build())
                .addFileComment(this.javaPoetConfig.licenseComment())
                .skipJavaLangImports(true)
                .indent(INDENT)
                .build();

        return this;
    }

    /**
     * JavaFileを出力します.
     *
     * @param processingEnv 出力先となるAnnotationProcessor の実行環境情報
     */
    public void writeTo(ProcessingEnvironment processingEnv) {
        if (this.javaFile == null) {
            throw new JavaPoetException("JavaPoet's JavaFile does not set.");
        }
        try {
            this.javaFile.writeTo(processingEnv.getFiler());
        } catch (FilerException ex) {
            //重複ファイルが存在している場合なので無視する
        } catch (IOException | RuntimeException ex) {
            throw new JavaPoetException(ex, "JavaPoet's JavaFile can not write to annotation processor filer.");
        }
    }

    /**
     * 保持しているJavaFileの文字列情報を返却します.
     *
     * @return 保持しているJavaFileの文字列情報
     */
    @Override
    public String toString() {
        return this.javaFile != null ? this.javaFile.toString() : "";
    }
}
