/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.javapoet;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.CodeBlock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Generated;
import org.vermeerlab.apt.command.ProcessorCommandInterface;

/**
 * JavaPoet による編集用の基本定義情報を扱うクラスです.
 *
 * @author Yamashita,Takahiro
 */
public class JavaPoetConfig {

    private String licenseTemplate;
    private String copyRightYear;
    private String ownerName;
    private String classJavaDoc;
    private List<String> sinces;
    private List<String> versions;
    private List<String> authors;
    private String generatedClass;
    private List<?> classJavaDocParams;

    public JavaPoetConfig(String licenseTemplate, String copyRightYear, String ownerName, String classJavaDoc, List<String> sinces, List<String> versions, List<String> authors, String generatedClass, List<?> classJavaDocParams) {
        this.licenseTemplate = licenseTemplate;
        this.copyRightYear = copyRightYear;
        this.ownerName = ownerName;
        this.classJavaDoc = classJavaDoc;
        this.sinces = sinces;
        this.versions = versions;
        this.authors = authors;
        this.generatedClass = generatedClass;
        this.classJavaDocParams = classJavaDocParams;
    }

    public JavaPoetConfig(String licenseTemplate, String copyRightYear, String ownerName, String classJavaDoc, List<String> sinces, List<String> versions, List<String> authors) {
        this(licenseTemplate, copyRightYear, ownerName, classJavaDoc, sinces, versions, authors, "",
             Collections.emptyList());
    }

    /**
     * 基本となる定義情報にコマンド別の定義情報をマージしたインスタンスを返却します.
     *
     * @param <T> 引数のコマンドクラスの型
     * @param extentionJavaPoetConfig 実行コマンド別の定義情報
     * @param command 実行コマンドクラス
     * @return 構築したインスタンス
     */
    public <T extends ProcessorCommandInterface> JavaPoetConfig mergeByCommand(JavaPoetConfig extentionJavaPoetConfig, T command) {
        String _generatedClass = command.getClass().getCanonicalName();

        if (this == extentionJavaPoetConfig) {
            return new JavaPoetConfig(this.licenseTemplate, this.copyRightYear, this.ownerName, this.classJavaDoc,
                                      this.sinces, this.versions, this.authors,
                                      _generatedClass, this.classJavaDocParams);
        }

        String _licenseTemplate = extentionJavaPoetConfig.licenseTemplate.equals("")
                                  ? this.licenseTemplate
                                  : extentionJavaPoetConfig.licenseTemplate;

        String _copyRightYear = extentionJavaPoetConfig.copyRightYear.equals("")
                                ? this.copyRightYear
                                : extentionJavaPoetConfig.copyRightYear;

        String _ownerName = extentionJavaPoetConfig.ownerName.equals("")
                            ? this.ownerName
                            : extentionJavaPoetConfig.ownerName;

        String _classJavaDoc = extentionJavaPoetConfig.classJavaDoc.equals("")
                               ? this.classJavaDoc
                               : extentionJavaPoetConfig.classJavaDoc;

        List<String> _sinces = new ArrayList<>();
        _sinces.addAll(this.sinces);
        _sinces.addAll(extentionJavaPoetConfig.sinces);

        List<String> _versions = new ArrayList<>();
        _versions.addAll(this.versions);
        _versions.addAll(extentionJavaPoetConfig.versions);

        List<String> _authors = new ArrayList<>();
        _authors.addAll(this.authors);
        _authors.addAll(extentionJavaPoetConfig.authors);

        return new JavaPoetConfig(_licenseTemplate, _copyRightYear, _ownerName,
                                  _classJavaDoc,
                                  _sinces, _versions, _authors, _generatedClass,
                                  this.classJavaDocParams);
    }

    /**
     * ClassのJavaDocにTitleを付与したインスタンスを返却します.
     * <P>
     * {@literal "$S"}（JavaPoetによる置換指定ルールに則って）を置換文字で変換します.
     *
     * @param classJavaDocTitle 追記するJavaDocのTitleコメント.
     * @param replaceChars クラスコメント内にある {@literal "$S"} を置換するための文字列（指定純に適用）
     * @return ClassのJavaDocにTitleを付与したインスタンス
     */
    public JavaPoetConfig mergeClassJavaDocComment(String classJavaDocTitle, String... replaceChars) {
        String _classJavaDoc = classJavaDocTitle == null || classJavaDocTitle.equals("")
                               ? this.classJavaDoc
                               : classJavaDocTitle + "\n<P>\n" + this.classJavaDoc;

        List<String> _classJavaDocParams = Arrays.asList(replaceChars);

        return new JavaPoetConfig(this.licenseTemplate, this.copyRightYear, this.ownerName,
                                  _classJavaDoc,
                                  this.sinces, this.versions, this.authors, this.generatedClass,
                                  _classJavaDocParams);
    }

    /**
     * クラスヘッダに出力するLicenseに関するJavaDocを返却します.
     *
     * @return Licenseに関するJavaDoc
     */
    public String licenseComment() {
        String _licenseTemplate = this.licenseTemplate;
        if (_licenseTemplate.isEmpty()) {
            return "";
        }

        String licenseComment = _licenseTemplate
                .replace("[CopyRightYear]", this.copyRightYear)
                .replace("[name of copyright owner]", this.ownerName);
        return licenseComment;
    }

    /**
     * クラス生成に関連するクラス表記のアノテーション（{@link javax.annotation.Generated}）に
     * 付与する情報を返却します.
     *
     * @return クラス生成に関するクラス情報
     */
    public AnnotationSpec classGeneratedAnnotationSpec() {
        Set<String> classes = new LinkedHashSet<>();
        classes.add("org.vermeerlab.apt.AnnotationProcessorController");
        if (this.generatedClass.equals("") == false) {
            classes.add(this.generatedClass);
        }

        List<String> formatList = classes.stream()
                .map(dummy -> "$S").collect(Collectors.toList());

        String format = "{" + String.join(",", formatList) + "}";

        AnnotationSpec annotationSpec = AnnotationSpec
                .builder(Generated.class)
                .addMember("value", format, classes.toArray())
                .build();

        return annotationSpec;
    }

    /**
     * クラスコメントを返却します.
     *
     * @return クラスコメント
     */
    public CodeBlock classJavaDocCodeBlock() {
        CodeBlock.Builder codeBuilder = CodeBlock.builder();
        String _classJavaDoc = this.classJavaDoc;
        if (this.classJavaDocParams.isEmpty()) {
            codeBuilder.add(_classJavaDoc);
        } else {
            codeBuilder.add(_classJavaDoc, this.classJavaDocParams);
        }
        return codeBuilder.build();
    }

    /**
     * クラスコメントに付与するアノテーションを返却します.
     * <P>
     * {@code @author}、{@code @since}、{@code @version} を編集します.
     *
     * @return クラスコメントに付与するアノテーション
     */
    public CodeBlock classComment() {
        CodeBlock.Builder codeBuilder = CodeBlock.builder();
        codeBuilder.add(this.classJavaDocCodeBlock());

        codeBuilder.add("\n");

        this.sinces.stream()
                .forEachOrdered(e -> codeBuilder.add("@since $L\n", e));
        this.versions.stream()
                .forEachOrdered(e -> codeBuilder.add("@version $L\n", e));
        this.authors.stream()
                .forEachOrdered(e -> codeBuilder.add("@author $L\n", e));

        return codeBuilder.build();
    }
}
