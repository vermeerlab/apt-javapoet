/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.javapoet;

import com.squareup.javapoet.CodeBlock;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class JavaPoetConfigHandlerTest {

    private String licenseTemplate;
    private String copyRightYear;
    private String ownerName;
    private String classJavaDoc;
    private List<String> sinces;
    private List<String> versions;
    private List<String> authors;
    private String generatedClass;
    private List<?> classJavaDocParams;

    @Before
    public void setUp() {
        licenseTemplate = "";
        copyRightYear = "";
        ownerName = "";
        classJavaDoc = "";
        sinces = Collections.emptyList();
        versions = Collections.emptyList();
        authors = Collections.emptyList();
        generatedClass = "";
        classJavaDocParams = Collections.emptyList();
    }

    @Test
    public void testLicenseCommentBlack() {
        JavaPoetConfig javaPoetConfig = new JavaPoetConfig(licenseTemplate, copyRightYear, ownerName, classJavaDoc,
                                                           sinces, versions, authors, generatedClass, classJavaDocParams);
        Assert.assertEquals("", javaPoetConfig.licenseComment());
    }

    @Test
    public void testLicenseComment() {
        licenseTemplate = "licenseTemplate";
        JavaPoetConfig javaPoetConfig = new JavaPoetConfig(licenseTemplate, copyRightYear, ownerName, classJavaDoc,
                                                           sinces, versions, authors, generatedClass, classJavaDocParams);
        Assert.assertEquals("licenseTemplate", javaPoetConfig.licenseComment());
    }

    public void testLicenseCommentWithReplace() {
        licenseTemplate = "licenseTemplate [CopyRightYear]  [name of copyright owner]";
        copyRightYear = "9999";
        ownerName = "ownerName";

        JavaPoetConfig javaPoetConfig = new JavaPoetConfig(licenseTemplate, copyRightYear, ownerName, classJavaDoc,
                                                           sinces, versions, authors, generatedClass, classJavaDocParams);
        Assert.assertEquals("licenseTemplate 9999 ownerName", javaPoetConfig.licenseComment());
    }

    @Test
    public void testClassCommentAnnotation() {
        JavaPoetConfig javaPoetConfig = new JavaPoetConfig(licenseTemplate, copyRightYear, ownerName, classJavaDoc,
                                                           sinces, versions, authors, generatedClass, classJavaDocParams);
        CodeBlock codeBlock = javaPoetConfig.classComment();
        Assert.assertEquals("\n", codeBlock.toString());
    }

    @Test
    public void testClassCommentAnnotationWithReplace() {
        authors = Arrays.asList("AA", "BB", "CC");
        versions = Arrays.asList("v1", "v2", "v3");
        sinces = Arrays.asList("1.1", "1.2", "1.3");

        JavaPoetConfig javaPoetConfig = new JavaPoetConfig(licenseTemplate, copyRightYear, ownerName, classJavaDoc,
                                                           sinces, versions, authors, generatedClass, classJavaDocParams);

        CodeBlock codeBlock = javaPoetConfig.classComment();
        Assert.assertEquals("\n"
                            + "@since 1.1\n"
                            + "@since 1.2\n"
                            + "@since 1.3\n"
                            + "@version v1\n"
                            + "@version v2\n"
                            + "@version v3\n"
                            + "@author AA\n"
                            + "@author BB\n"
                            + "@author CC\n"
                            + "", codeBlock.toString());
    }

}
