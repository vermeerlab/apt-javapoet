/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.javapoet;

import com.squareup.javapoet.TypeSpec;
import java.util.Locale;
import java.util.Map;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.junit.Assert;
import org.junit.Test;

public class JavaPoetFileTest {

    static class TestProcessingEnvironment implements ProcessingEnvironment {

        @Override
        public Map<String, String> getOptions() {
            return null;
        }

        @Override
        public Messager getMessager() {
            return null;
        }

        @Override
        public Filer getFiler() {
            return null;
        }

        @Override
        public Elements getElementUtils() {
            return null;
        }

        @Override
        public Types getTypeUtils() {
            return null;
        }

        @Override
        public SourceVersion getSourceVersion() {
            return null;
        }

        @Override
        public Locale getLocale() {
            return null;
        }
    }

    @Test(expected = JavaPoetException.class)
    public void javaFileIsNull() {
        JavaPoetFile javaPoetFile = JavaPoetFile.of();
        javaPoetFile.writeTo(null);
    }

    @Test(expected = JavaPoetException.class)
    public void javaFileの出力先でIOException() {
        TestProcessingEnvironment processingEnvironment = new TestProcessingEnvironment() {
            @Override
            public Filer getFiler() {
                throw new RuntimeException();
            }
        };

        TypeSpec.Builder typeBuilder = TypeSpec.classBuilder("Test");
        JavaPoetFile javaPoetFile = JavaPoetFile.of();

        javaPoetFile.setCode(
                "", typeBuilder);
        javaPoetFile.writeTo(processingEnvironment);
    }

    @Test
    public void testToString() {
        TypeSpec.Builder typeBuilder = TypeSpec.classBuilder("Test");
        JavaPoetFile javaPoetFile = JavaPoetFile.of();
        javaPoetFile.setCode("", typeBuilder);
        String test = javaPoetFile.toString();

        String expect
               = "import javax.annotation.Generated;\n"
                 + "\n"
                 + "/**\n"
                 + " *\n"
                 + " */\n"
                 + "@Generated({\"org.vermeerlab.apt.AnnotationProcessorController\"})\n"
                 + "class Test {\n"
                 + "}\n"
                 + "";

        Assert.assertEquals(expect, test);
    }

    @Test
    public void コンストラクタカバレッジ() {
        JavaPoetFile javaPoetFile = new JavaPoetFile();
    }

}
