/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorException;

/**
 *
 * @author Yamashita,Takahiro
 */
public class BaseElementTest {

    private ProcessingEnvironment processingEnv;

    class TestElement implements Element {

        @Override
        public TypeMirror asType() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public ElementKind getKind() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Set<Modifier> getModifiers() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Name getSimpleName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Element getEnclosingElement() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends Element> getEnclosedElements() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends AnnotationMirror> getAnnotationMirrors() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <R, P> R accept(ElementVisitor<R, P> v, P p) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Before
    public void setUp() {
        processingEnv = new ProcessingEnvironment() {
            @Override
            public Map<String, String> getOptions() {
                return null;
            }

            @Override
            public Messager getMessager() {
                return null;
            }

            @Override
            public Filer getFiler() {
                return null;
            }

            @Override
            public Elements getElementUtils() {
                return null;
            }

            @Override
            public Types getTypeUtils() {
                return null;
            }

            @Override
            public SourceVersion getSourceVersion() {
                return null;
            }

            @Override
            public Locale getLocale() {
                return null;
            }
        };
    }

    @Test
    public void 疎通_分類判定() {
        BaseElement baseElement = BaseElement.of(this.processingEnv, null, null);
        Assert.assertThat(baseElement.getTargetAnnotation(), is(Optional.empty()));
        Assert.assertThat(CategorizedElementKind.OTHER.isAssignableFrom(baseElement), is(true));
        Assert.assertThat(CategorizedElementKind.CLASS.isAssignableFrom(baseElement), is(false)
        );
    }

    @Test(expected = AnnotationProcessorException.class)
    public void children_annotation_is_null() {
        BaseElement baseElement = BaseElement.of(this.processingEnv, null, null);
        baseElement.children(null);
    }

    @Test
    public void testHashCodeNull() {
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, null, null);
        BaseElement categorizedElement2 = BaseElement.of(this.processingEnv, null, null);
        Assert.assertEquals(categorizedElement1, categorizedElement2);
        Assert.assertEquals(categorizedElement1.hashCode(), categorizedElement2.hashCode());
    }

    @Test
    public void コンストラクタ_カバレッジ対応() {
        BaseElement element = new BaseElement();
    }

    @Test
    public void コンストラクタ引数アリ_カバレッジ対応() {
        BaseElement base = new BaseElement();
        BaseElement element = new BaseElement(base);
    }

    @Test
    public void testHashCode() {

        Element element1 = new TestElement() {
            @Override
            public int hashCode() {
                return 1;
            }
        };

        Element element2 = new TestElement() {
            @Override
            public int hashCode() {
                return 2;
            }
        };

        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, element1, null);
        BaseElement categorizedElement2 = BaseElement.of(this.processingEnv, element2, null);
        Assert.assertNotEquals(categorizedElement1, categorizedElement2);

        Element element3 = new TestElement() {
            @Override
            public int hashCode() {
                return 2;
            }
        };
        BaseElement categorizedElement3 = BaseElement.of(this.processingEnv, element3, null);
        Assert.assertEquals(categorizedElement2.hashCode(), categorizedElement3.hashCode());
    }

    @Test
    public void testEquals() {
        Element element1 = new TestElement() {
            @Override
            public int hashCode() {
                return 1;
            }
        };

        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, element1, null);
        Assert.assertTrue(categorizedElement1.equals(categorizedElement1));
        Assert.assertFalse(categorizedElement1.equals(null));
    }

    @Test
    public void testNotEqualsClass() {
        Element element1 = new TestElement() {
            @Override
            public int hashCode() {
                return 1;
            }
        };

        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, element1, null);
        Assert.assertFalse(categorizedElement1.equals(1));
    }
}
