/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element.test.command.method;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.apt.element.CategorizedElementInterface;
import org.vermeerlab.apt.element.CategorizedElementKind;
import org.vermeerlab.apt.element.CategorizedElementsInterface;
import org.vermeerlab.apt.element.ClassElement;
import org.vermeerlab.apt.element.MethodElements;
import org.vermeerlab.base.DebugUtil;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ClassWithMethodTestCommand implements ProcessorCommandInterface {

    @Override
    public Class<? extends Annotation> getTargetAnnotation() {
        return ElementClassWithMethod.class;
    }

    @Override
    public void execute(ProcessingEnvironment processingEnvironment, Element element, Boolean isDebug) {
        ClassElement classElement = CategorizedElementKind.createCategorizedElement(
                processingEnvironment, element, this.getTargetAnnotation());
        DebugUtil debugUtil = new DebugUtil(isDebug);

        CategorizedElementsInterface _elements = classElement.children();

        MethodElements methodElements = _elements.filter(ElementKind.METHOD);

        debugUtil.print(
                "Class Annotation Name = " + classElement.getTargetAnnotation().get().getSimpleName()
                + ":method element count = " + String.valueOf(methodElements.values().size())
                + ":validateTargetFieldOneOrLess = " + methodElements.validateTargetFieldOneOrLess().isValid()
                + ":validateReturnTypeIsSame = " + methodElements.validateReturnTypeIsSame(String.class).isValid()
        );

        List<CategorizedElementsInterface> methodChildren = _elements.values().stream()
                .map(CategorizedElementInterface::children)
                .map(children -> (CategorizedElementsInterface) children)
                .collect(Collectors.toList());

        debugUtil.print(
                "methodChildren count = " + String.valueOf(methodChildren.size())
        );

        List<String> methodblocks = methodElements.values().stream()
                .filter(v -> v.getSimpleName().equals("methodTest21"))
                .map(v -> {
                    return v.getMethodBlock();
                }).collect(Collectors.toList());

        if (methodblocks.isEmpty() == false) {
            debugUtil.print(
                    ":methodBlock = " + methodblocks.get(0)
            );

        }
    }
}
