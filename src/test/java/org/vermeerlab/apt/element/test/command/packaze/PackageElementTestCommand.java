/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element.test.command.packaze;

import java.lang.annotation.Annotation;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.apt.element.CategorizedElementKind;
import org.vermeerlab.apt.element.CategorizedElementsInterface;
import org.vermeerlab.apt.element.PackageElement;
import org.vermeerlab.base.DebugUtil;

/**
 *
 * @author Yamashita,Takahiro
 */
public class PackageElementTestCommand implements ProcessorCommandInterface {

    @Override
    public Class<? extends Annotation> getTargetAnnotation() {
        return ElementPackage.class;
    }

    @Override
    public void execute(ProcessingEnvironment processingEnvironment, Element element, Boolean isDebug) {
        PackageElement packageElement = CategorizedElementKind.createCategorizedElement(
                processingEnvironment, element, this.getTargetAnnotation());
        DebugUtil debugUtil = new DebugUtil(isDebug);

        CategorizedElementsInterface mixElements = packageElement.children(ElementMethodWithPackage.class);
        debugUtil.print(
                "ElementMethodWithPackage:"
                + packageElement.getTargetAnnotation().get().getSimpleName()
                + String.valueOf(mixElements.values().size())
        );

        CategorizedElementsInterface classElements = packageElement.children(ElementClassWithPackage.class);
        debugUtil.print(
                "ElementClassWithPackage:"
                + packageElement.getTargetAnnotation().get().getSimpleName()
                + String.valueOf(classElements.values().size())
        );
        debugUtil.print(
                ":getAnnotation = " + packageElement.getAnnotation(ElementPackage.class).get().toString() + ","
                + ":getElement = " + packageElement.getElement().toString() + ","
                + ":getPackagePath = " + packageElement.getPackagePath() + ","
                + ":getSimpleName = " + packageElement.getSimpleName() + ","
                + ":children isEmpty = " + packageElement.children().isEmpty() + ","
        );
    }
}
