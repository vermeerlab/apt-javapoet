package org.vermeerlab.apt.element.test.command.field;

import com.google.common.io.Resources;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourceSubjectFactory;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorController;

public class FieldElementsTestCommandTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.out;
        System.setOut(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After public void tearDown() {
        System.setOut(_out);
    }

    private String resultValue() {
        String result = _baos.toString();
        return result == null
               ? ""
               : result.replaceAll("\r|\n", "");
    }

    @Test
    public void クラスに同じ値のフィールドが２つ() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/field/Test2.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();

        System.out.flush();
        String expect = "Annotation Class Name = ElementClassWithField"
                        + ":validateOneOrLess  = false"
                        + ":isEmpty = false:field children is empty = true"
                        + ":validateRequire = true"
                        + ":isDuplicateValid = false"
                        + ":qualifiedName = org.vermeerlab.apt.element.test.command.field.Test2";
        assertTrue(resultValue().contains(expect));
    }

    @Test
    public void クラスに違う値のフィールドが２つ() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/field/Test3.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();

        System.out.flush();
        String expect = "Annotation Class Name = ElementClassWithField"
                        + ":validateOneOrLess  = false"
                        + ":isEmpty = false:field children is empty = true"
                        + ":validateRequire = true"
                        + ":isDuplicateValid = true"
                        + ":qualifiedName = org.vermeerlab.apt.element.test.command.field.Test3";
        assertTrue(resultValue().contains(expect));
    }

    @Test
    public void クラスにフィールドが存在しない() {
        try {
            Truth.assert_()
                    .about(JavaSourceSubjectFactory.javaSource())
                    .that(JavaFileObjects.forResource(Resources.getResource(
                            "org/vermeerlab/apt/element/test/command/field/Test4.java"
                    )))
                    .processedWith(new AnnotationProcessorController(true))
                    .compilesWithoutError();

            Assert.assertThat("Test Error. Do not reach this root", is(""));
        } catch (java.lang.AssertionError e) {
            Assert.assertThat(e.getMessage(), containsString("No Field"));
        }
    }
}
