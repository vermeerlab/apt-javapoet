/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element.test.command.extentioncommand;

import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.TypeSpec;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import org.vermeerlab.apt.config.CommandConfigScannerInterface;
import org.vermeerlab.apt.javapoet.AbstractJavaPoetProcessorCommand;
import org.vermeerlab.apt.javapoet.JavaPoetFile;
import org.vermeerlab.base.DebugUtil;

/**
 *
 * @author Yamashita,Takahiro
 */
public class GenerateExtentionCommand extends AbstractJavaPoetProcessorCommand {

    @Override
    public Class<? extends Annotation> getTargetAnnotation() {
        return GenerateExtention.class;
    }

    @Override
    public void execute(ProcessingEnvironment processingEnvironment, Element element, Boolean isDebug) {
        //Elementの情報を使わずコード生成のみを行う.
        JavaPoetFile javaPoetFile = JavaPoetFile.of(this);
        TypeSpec.Builder typeBuilder = TypeSpec.classBuilder("Result");

        GenerateExtentionCommandScanner scanner
                                        = this.getCommandConfigScanner(GenerateExtentionCommandScanner.class).get();

        FieldSpec fieldSpec = FieldSpec
                .builder(String.class, "EXTENTION_VALUE", Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                .initializer("$S", scanner.getValue()).build();

        typeBuilder.addField(fieldSpec);

        javaPoetFile.setCode("org.vermeerlab.apt.element.test.command.extentioncommand", typeBuilder).writeTo(
                processingEnvironment);

        DebugUtil debugUtil = new DebugUtil(isDebug);
        debugUtil.print(javaPoetFile.toString());
    }

    @Override
    protected List<Class<? extends CommandConfigScannerInterface>> getExtentionCommandConfigScannerClasses() {
        return Arrays.asList(GenerateExtentionCommandScanner.class);
    }

}
