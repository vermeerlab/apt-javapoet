/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element.test.command.field;

import java.lang.annotation.Annotation;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.apt.element.CategorizedElementKind;
import org.vermeerlab.apt.element.CategorizedElementsInterface;
import org.vermeerlab.apt.element.FieldElement;
import org.vermeerlab.base.DebugUtil;

/**
 *
 * @author Yamashita,Takahiro
 */
public class FieldElementTestCommand implements ProcessorCommandInterface {

    @Override
    public Class<? extends Annotation> getTargetAnnotation() {
        return ElementField.class;
    }

    @Override
    public void execute(ProcessingEnvironment processingEnvironment, Element element, Boolean isDebug) {
        FieldElement fieldElement = CategorizedElementKind.createCategorizedElement(
                processingEnvironment, element, this.getTargetAnnotation());
        CategorizedElementsInterface _element = fieldElement.children(this.getTargetAnnotation());
        DebugUtil debugUtil = new DebugUtil(isDebug);

        String value = fieldElement.getConstantValue().orElse("null");

        debugUtil.print(
                fieldElement.getTargetAnnotation().get().getSimpleName()
                + String.valueOf(_element.values().size())
                + ":fieldValue = " + value
        );
    }
}
