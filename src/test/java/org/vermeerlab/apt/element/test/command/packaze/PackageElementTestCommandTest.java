package org.vermeerlab.apt.element.test.command.packaze;

import com.google.common.io.Resources;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourceSubjectFactory;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.*;

public class PackageElementTestCommandTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.out;
        System.setOut(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After
    public void tearDown() {
        System.setOut(_out);
    }

    private String resultValue() {
        String result = _baos.toString();
        return result == null
               ? ""
               : result.replaceAll("\r|\n", "");
    }

    @Test
    public void 注釈情報の参照() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/packaze/package-info.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();

        System.out.flush();

        String expect
               //メソッドはクラス配下なのでパッケージ配下としてはカウントされない
               = "ElementMethodWithPackage:ElementPackage0"
                 //クラスはパッケージ配下の要素としてカウントされる
                 + "ElementClassWithPackage:ElementPackage1"
                 //その他
                 + ":getAnnotation = @org.vermeerlab.apt.element.test.command.packaze.ElementPackage(),"
                 + ":getElement = org.vermeerlab.apt.element.test.command.packaze,"
                 + ":getPackagePath = org.vermeerlab.apt.element.test.command.packaze,"
                 + ":getSimpleName = packaze,"
                 + ":children isEmpty = false,";
        assertTrue(resultValue().contains(expect));
    }
}
