package org.vermeerlab.apt.element.test.command.method;

import com.google.common.io.Resources;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourceSubjectFactory;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorController;

public class ClassWithMethodTestCommandTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.out;
        System.setOut(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After public void tearDown() {
        System.setOut(_out);
    }

    private String resultValue() {
        String result = _baos.toString();
        return result == null
               ? ""
               : result.replaceAll("\r|\n", "");
    }

    @Test
    public void 疎通() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/method/Test2.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();
    }

    @Test
    public void クラス配下のメソッド要素を取得() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/method/Test2.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();

        System.out.flush();
        String expect = "Class Annotation Name = ElementClassWithMethod"
                        + ":method element count = 2"
                        + ":validateTargetFieldOneOrLess = false"
                        + ":validateReturnTypeIsSame = false"
                        + "methodChildren count = 3"
                        + ":methodBlock = return \"testMethod\";";
        assertTrue(resultValue().contains(expect));
    }
}
