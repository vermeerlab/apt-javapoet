package org.vermeerlab.apt.element.test.command.other;

import com.google.common.io.Resources;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourceSubjectFactory;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorController;

public class OtherElementTestCommandTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.out;
        System.setOut(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After public void tearDown() {
        System.setOut(_out);
    }

    private String resultValue() {
        String result = _baos.toString();
        return result == null
               ? ""
               : result.replaceAll("\r|\n", "");
    }

    @Test
    public void クラスに対象フィールドが複数ある() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/other/Test1.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();

        System.out.flush();

        String result = resultValue();
        assertTrue(result.contains(
                "packageName:org.vermeerlab.apt.element.test.command.other:simpleName:Test1:hasAnnotation:true:childrenCount:2"));

        assertTrue(result.contains(
                "packageName:org.vermeerlab.apt.element.test.command.other:simpleName:fieldTest1:hasAnnotation:true:childrenCount:0"));

        assertTrue(result.contains(
                "packageName:org.vermeerlab.apt.element.test.command.other:simpleName:methodTest1:hasAnnotation:true:childrenCount:0"));

    }
}
