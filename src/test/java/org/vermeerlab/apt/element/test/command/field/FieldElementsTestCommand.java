/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element.test.command.field;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.AnnotationProcessorException;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.apt.element.CategorizedElementKind;
import org.vermeerlab.apt.element.CategorizedElementsInterface;
import org.vermeerlab.apt.element.ClassElement;
import org.vermeerlab.apt.element.FieldElements;
import org.vermeerlab.base.DebugUtil;

/**
 *
 * @author Yamashita,Takahiro
 */
public class FieldElementsTestCommand implements ProcessorCommandInterface {

    @Override
    public Class<? extends Annotation> getTargetAnnotation() {
        return ElementClassWithField.class;
    }

    @Override
    public void execute(ProcessingEnvironment processingEnvironment, Element element, Boolean isDebug) {
        ClassElement classElement = CategorizedElementKind.createCategorizedElement(
                processingEnvironment, element, this.getTargetAnnotation());

        DebugUtil debugUtil = new DebugUtil(isDebug);

        CategorizedElementsInterface mixingPossibilityElements = classElement.children();
        if (mixingPossibilityElements.isEmpty()) {
            throw new AnnotationProcessorException("No Field");
        }

        FieldElements fieldElements = mixingPossibilityElements.filter(ElementKind.FIELD);

        String filterValue = "sameValue";
        FieldElements filteredFieldElements1 = fieldElements.filterConstantValue(filterValue);

        Set<String> filterValueSet = new HashSet<>();
        filterValueSet.add("differentValue1");
        filterValueSet.add("differentValue2");
        FieldElements filteredFieldElements2 = fieldElements.filterConstantValue(filterValueSet);

        FieldElements filteredFieldElements = filteredFieldElements1.isEmpty()
                                              ? filteredFieldElements2
                                              : filteredFieldElements1;

        String fieldQualifiedName = fieldElements.values().get(0).getQualifiedName();

        debugUtil.print(
                "Annotation Class Name = " + classElement.getTargetAnnotation().get().getSimpleName()
                + ":validateOneOrLess  = " + String.valueOf(
                        filteredFieldElements.validateTargetFieldOneOrLess().isValid())
                + ":isEmpty = " + String.valueOf(filteredFieldElements.isEmpty())
                + ":field children is empty = " + fieldElements.values().get(0).children().isEmpty()
                + ":validateRequire = " + fieldElements.validateRequire().isValid()
                + ":isDuplicateValid = " + filteredFieldElements.validateDuplicate().isValid()
                + ":qualifiedName = " + fieldQualifiedName
        );
    }
}
