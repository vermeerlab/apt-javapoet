package org.vermeerlab.apt.element.test.command.method;

import com.google.common.io.Resources;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourceSubjectFactory;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorController;

public class MethodElementTestCommandTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.out;
        System.setOut(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After public void tearDown() {
        System.setOut(_out);
    }

    private String resultValue() {
        String result = _baos.toString();
        return result == null
               ? ""
               : result.replaceAll("\r|\n", "");
    }

    @Test
    public void 疎通() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/method/Test3.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();
    }

    @Test
    public void メソッドの戻り値の型がvoidの実行結果の動的取得は例外() {
        try {
            Truth.assert_()
                    .about(JavaSourceSubjectFactory.javaSource())
                    .that(JavaFileObjects.forResource(Resources.getResource(
                            "org/vermeerlab/apt/element/test/command/method/Test1.java"
                    )))
                    .processedWith(new AnnotationProcessorController(true))
                    .compilesWithoutError();
        } catch (java.lang.AssertionError e) {
            Assert.assertThat(e.getMessage(), containsString("▼▼ report start ▼▼"));
        }
    }

    @Test
    public void メソッド配下の要素数を取得() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/method/Test3.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();

        System.out.flush();
        String expect = "QualifiedName = org.vermeerlab.apt.element.test.command.method.Test3"
                        + ":Annotation SimpleName = TargetMethodElement"
                        + ":value count = 0"
                        + ":executeResult = " + "methodResultValue";
        assertTrue(resultValue().contains(expect));
    }
}
