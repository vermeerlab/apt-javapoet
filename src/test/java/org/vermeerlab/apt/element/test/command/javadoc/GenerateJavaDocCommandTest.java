package org.vermeerlab.apt.element.test.command.javadoc;

import com.google.common.io.Resources;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourceSubjectFactory;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorController;
import org.vermeerlab.compiler.SourceFileReader;

public class GenerateJavaDocCommandTest {

    @Test
    public void 疎通() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/javadoc/Test.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command-javadoc.xml"))
                .compilesWithoutError();
    }

    @Test
    public void 結果確認() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/javadoc/Test.java"
                )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/element/test/command/javadoc/Result.java"
                )).toJavaFileObject());
    }
}
