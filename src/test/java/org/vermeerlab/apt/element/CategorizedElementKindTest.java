/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import org.junit.Test;
import org.vermeerlab.apt.javapoet.JavaPoetException;

/**
 *
 * @author Yamashita,Takahiro
 */
public class CategorizedElementKindTest {

    @Test(expected = JavaPoetException.class)
    public void createCategorizedElement_必須項目テスト() {
        CategorizedElementKind.createCategorizedElement(null, null, null);
    }

    @Test(expected = JavaPoetException.class)
    public void createCategorizedElements_必須項目テスト() {
        CategorizedElementKind.createCategorizedElements(null, null);
    }
}
