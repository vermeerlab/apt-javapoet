/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ClassElementsTest {

    private ProcessingEnvironment processingEnv;

    class TestElement implements Element {

        @Override
        public TypeMirror asType() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.CLASS;
        }

        @Override
        public Set<Modifier> getModifiers() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Name getSimpleName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Element getEnclosingElement() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends Element> getEnclosedElements() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends AnnotationMirror> getAnnotationMirrors() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <R, P> R accept(ElementVisitor<R, P> v, P p) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Before
    public void setUp() {
        processingEnv = new ProcessingEnvironment() {
            @Override
            public Map<String, String> getOptions() {
                return null;
            }

            @Override
            public Messager getMessager() {
                return null;
            }

            @Override
            public Filer getFiler() {
                return null;
            }

            @Override
            public Elements getElementUtils() {
                return null;
            }

            @Override
            public Types getTypeUtils() {
                return null;
            }

            @Override
            public SourceVersion getSourceVersion() {
                return null;
            }

            @Override
            public Locale getLocale() {
                return null;
            }
        };
    }

    @Test
    public void 疎通() {
        List<ClassElement> classElementList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            ClassElement classElement = new ClassElement();
            classElementList.add(classElement);
        }
        ClassElements classElements = new ClassElements(BaseElements.of(classElementList, null));
        Assert.assertThat(classElements.getTargetAnnotation(), is(Optional.empty()));
        Assert.assertThat(classElements.isEmpty(), is(false));
        Assert.assertThat(classElements.values().size(), is(2));
    }

    @Test
    public void コンストラクタ_カバレッジ対応() {
        ClassElements element = new ClassElements();
    }

    @Test
    public void コンストラクタ_パラメータあり_カバレッジ対応() {
        ClassElements element = new ClassElements(new ClassElements());
    }

    @Test
    public void testFilter() {
        Element element1 = new TestElement() {
        };
        BaseElement baseElement = BaseElement.of(this.processingEnv, element1, null);
        ClassElement classElement = new ClassElement(baseElement);
        List<ClassElement> classElementItems = new ArrayList<>();
        classElementItems.add(classElement);

        ClassElements classElements = new ClassElements(BaseElements.of(classElementItems, null));
        ClassElements filteredClassElements = classElements.filter(ElementKind.CLASS);
        Assert.assertThat(CategorizedElementKind.CLASS.isAllAssignableFrom(filteredClassElements), is(true));
    }

    @Test
    public void testHashCode() {
        Element element1 = new TestElement() {
        };
        List<ClassElement> items = new ArrayList<>();
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, element1, null);
        ClassElement classElement1 = new ClassElement(categorizedElement1);
        items.add(classElement1);
        Class<? extends Annotation> annotation = null;
        ClassElements classElements1 = new ClassElements(BaseElements.of(items, null));
        ClassElements classElements2 = new ClassElements(BaseElements.of(items, null));
        Assert.assertEquals(classElements1.hashCode(), classElements2.hashCode());
    }

    @Test
    public void testEquals() {
        Element element1 = new TestElement() {
        };
        List<ClassElement> items = new ArrayList<>();
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, element1, null);
        ClassElement classElement1 = new ClassElement(categorizedElement1);
        items.add(classElement1);
        Class<? extends Annotation> annotation = null;
        ClassElements classElements1 = new ClassElements(BaseElements.of(items, null));
        ClassElements classElements2 = new ClassElements(BaseElements.of(items, null));
        Assert.assertTrue(classElements1.equals(classElements1));
        Assert.assertTrue(classElements1.equals(classElements2));
        Assert.assertFalse(classElements1.equals(null));
        Assert.assertFalse(classElements1.equals(1));
    }
}
