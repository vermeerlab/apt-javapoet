/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class FieldElementsTest {

    private ProcessingEnvironment processingEnv;

    class TestElement implements Element {

        @Override
        public TypeMirror asType() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.FIELD;
        }

        @Override
        public Set<Modifier> getModifiers() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Name getSimpleName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Element getEnclosingElement() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends Element> getEnclosedElements() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends AnnotationMirror> getAnnotationMirrors() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <R, P> R accept(ElementVisitor<R, P> v, P p) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Before
    public void setUp() {
        processingEnv = new ProcessingEnvironment() {
            @Override
            public Map<String, String> getOptions() {
                return null;
            }

            @Override
            public Messager getMessager() {
                return null;
            }

            @Override
            public Filer getFiler() {
                return null;
            }

            @Override
            public Elements getElementUtils() {
                return null;
            }

            @Override
            public Types getTypeUtils() {
                return null;
            }

            @Override
            public SourceVersion getSourceVersion() {
                return null;
            }

            @Override
            public Locale getLocale() {
                return null;
            }
        };
    }

    @Test
    public void 疎通() {
        List<FieldElement> fieldElementList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            FieldElement fieldElement = new FieldElement();
            fieldElementList.add(fieldElement);
        }
        FieldElements fieldElements = new FieldElements(BaseElements.of(fieldElementList, null));
        Assert.assertThat(fieldElements.getTargetAnnotation(), is(Optional.empty()));
        Assert.assertThat(fieldElements.isEmpty(), is(false));
        Assert.assertThat(fieldElements.values().size(), is(2));
    }

    @Test
    public void コンストラクタ_カバレッジ対応() {
        FieldElements element = new FieldElements();
    }

    @Test
    public void コンストラクタ_パラメータあり_カバレッジ対応() {
        FieldElements element = new FieldElements(new FieldElements());
    }

    @Test
    public void testCanInstanceItemEmpty() {
        List<FieldElement> elementList = new ArrayList<>();
        FieldElements fieldElements = new FieldElements(BaseElements.of(elementList, null));
        Assert.assertThat(fieldElements.isEmpty(), is(true));
    }

    @Test
    public void testIs() {
        Element element1 = new TestElement() {
        };
        BaseElement baseElement = BaseElement.of(this.processingEnv, element1, null);
        FieldElement fieldElement = new FieldElement(baseElement);
        List<FieldElement> items = new ArrayList<>();
        items.add(fieldElement);
        FieldElements fieldElements = new FieldElements(BaseElements.of(items, null));
        Boolean isField = CategorizedElementKind.FIELD.isAllAssignableFrom(fieldElements);
        Assert.assertThat(isField, is(true));
    }

    @Test
    public void testFilter() {
        Element element1 = new TestElement() {
        };
        BaseElement baseElement = BaseElement.of(this.processingEnv, element1, null);
        FieldElement fieldElement = new FieldElement(baseElement);
        List<FieldElement> items = new ArrayList<>();
        items.add(fieldElement);
        FieldElements fieldElements = new FieldElements(BaseElements.of(items, null));
        FieldElements filteredElements = fieldElements.filter(ElementKind.FIELD);
        Assert.assertThat(CategorizedElementKind.FIELD.isAllAssignableFrom(filteredElements), is(true));
    }

    @Test
    public void testHashCode() {
        Element element1 = new TestElement() {
        };
        List<FieldElement> items = new ArrayList<>();
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, element1, null);
        FieldElement fieldElement1 = new FieldElement(categorizedElement1);
        items.add(fieldElement1);
        FieldElements fieldElements1 = new FieldElements(BaseElements.of(items, null));
        FieldElements fieldElements2 = new FieldElements(BaseElements.of(items, null));
        Assert.assertEquals(fieldElements1.hashCode(), fieldElements2.hashCode());
    }

    @Test
    public void testEquals() {
        Element element1 = new TestElement() {
        };
        List<FieldElement> items = new ArrayList<>();
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, element1, null);
        FieldElement fieldElement1 = new FieldElement(categorizedElement1);
        items.add(fieldElement1);
        FieldElements fieldElements1 = new FieldElements(BaseElements.of(items, null));
        FieldElements fieldElements2 = new FieldElements(BaseElements.of(items, null));
        Assert.assertTrue(fieldElements1.equals(fieldElements1));
        Assert.assertTrue(fieldElements1.equals(fieldElements2));
        Assert.assertFalse(fieldElements1.equals(null));
        Assert.assertFalse(fieldElements1.equals(1));
    }

}
