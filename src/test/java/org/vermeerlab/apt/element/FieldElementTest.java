/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class FieldElementTest {

    ProcessingEnvironment processingEnv;

    class TestElement implements Element {

        @Override
        public TypeMirror asType() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.FIELD;
        }

        @Override
        public Set<Modifier> getModifiers() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Name getSimpleName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Element getEnclosingElement() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends Element> getEnclosedElements() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends AnnotationMirror> getAnnotationMirrors() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <R, P> R accept(ElementVisitor<R, P> v, P p) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Before
    public void setUp() {
        processingEnv = new ProcessingEnvironment() {
            @Override
            public Map<String, String> getOptions() {
                return null;
            }

            @Override
            public Messager getMessager() {
                return null;
            }

            @Override
            public Filer getFiler() {
                return null;
            }

            @Override
            public Elements getElementUtils() {
                return null;
            }

            @Override
            public Types getTypeUtils() {
                return null;
            }

            @Override
            public SourceVersion getSourceVersion() {
                return null;
            }

            @Override
            public Locale getLocale() {
                return null;
            }
        };
    }

    @Test
    public void 疎通() {
        BaseElement baseElement = BaseElement.of(processingEnv, null, null);

        FieldElement fieldElement = new FieldElement(baseElement);
        Assert.assertThat(fieldElement.getTargetAnnotation(), is(Optional.empty()));
        Assert.assertThat(CategorizedElementKind.FIELD.isAssignableFrom(fieldElement), is(true));
        Assert.assertThat(CategorizedElementKind.OTHER.isAssignableFrom(fieldElement), is(false));
    }

    @Test
    public void コンストラクタ_カバレッジ対応() {
        FieldElement element = new FieldElement();
    }

    @Test
    public void コンストラクタ引数アリ_カバレッジ対応() {
        FieldElement fieldElement = new FieldElement();
        FieldElement element = new FieldElement(fieldElement);
    }

    @Test
    public void testHashCode() {
        Element element1 = new TestElement() {
        };
        BaseElement baseElement = BaseElement.of(processingEnv, element1, null);
        FieldElement fieldElement1 = new FieldElement(baseElement);
        FieldElement fieldElement2 = new FieldElement(baseElement);
        Assert.assertEquals(fieldElement1.hashCode(), fieldElement2.hashCode());
    }

    @Test
    public void testEquals() {
        Element element1 = new TestElement() {
        };
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, element1, null);
        FieldElement fieldElement1 = new FieldElement(categorizedElement1);

        BaseElement categorizedElement2 = BaseElement.of(this.processingEnv, element1, null);
        FieldElement fieldElement2 = new FieldElement(categorizedElement2);

        Assert.assertTrue(fieldElement1.equals(fieldElement1));
        Assert.assertTrue(fieldElement1.equals(fieldElement2));
        Assert.assertFalse(fieldElement1.equals(null));
        Assert.assertFalse(fieldElement1.equals(1));
    }

}
