/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.element;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.ValidationResult;

/**
 *
 * @author Yamashita,Takahiro
 */
public class BaseElementsTest {

    private ProcessingEnvironment processingEnv;

    class TestElement implements Element {

        @Override
        public TypeMirror asType() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.CLASS;
        }

        @Override
        public Set<Modifier> getModifiers() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Name getSimpleName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Element getEnclosingElement() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends Element> getEnclosedElements() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<? extends AnnotationMirror> getAnnotationMirrors() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <R, P> R accept(ElementVisitor<R, P> v, P p) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Before
    public void setUp() {
        processingEnv = new ProcessingEnvironment() {
            @Override
            public Map<String, String> getOptions() {
                return null;
            }

            @Override
            public Messager getMessager() {
                return null;
            }

            @Override
            public Filer getFiler() {
                return null;
            }

            @Override
            public Elements getElementUtils() {
                return null;
            }

            @Override
            public Types getTypeUtils() {
                return null;
            }

            @Override
            public SourceVersion getSourceVersion() {
                return null;
            }

            @Override
            public Locale getLocale() {
                return null;
            }
        };
    }

    @Test
    public void testOf() {
        BaseElement baseElement = BaseElement.of(this.processingEnv, null, null);
        List<BaseElement> items = new ArrayList<>();
        BaseElements categorizedElements = BaseElements.of(items, null);
    }

    @Test
    public void コンストラクタ_カバレッジ対応() {
        BaseElements element = new BaseElements();
    }

    @Test
    public void キャスト_カバレッジ対応() {
        BaseElements baseElement = new BaseElements();
        BaseElements element = new BaseElements().toCategorizedElements(baseElement);
    }

    @Test
    public void testFilter_対象要素が無い() {
        Element element = new TestElement() {
        };
        BaseElement baseElement = BaseElement.of(this.processingEnv, element, null);
        ClassElement classElement = new ClassElement(baseElement);
        List<ClassElement> classElementItems = new ArrayList<>();
        classElementItems.add(classElement);

        ClassElements classElements = new ClassElements(BaseElements.of(classElementItems, null));
        BaseElements baseElements = classElements.filter(ElementKind.OTHER);

        FieldElements fieldElements = classElements.filter(ElementKind.FIELD);

        try {
            FieldElements baseElementsException = classElements.filter(ElementKind.OTHER);
        } catch (Exception ex) {
            Assert.assertThat(ClassCastException.class.isAssignableFrom(ex.getClass()), is(true));
        }
    }

    @Test
    public void testValidateRequire() {
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, null, null);
        List<BaseElement> items = new ArrayList<>();

        BaseElements categorizedElements1 = BaseElements.of(items, null);
        ValidationResult result = categorizedElements1.validateRequire();
        Assert.assertFalse(result.isValid());

        items.add(categorizedElement1);
        BaseElements categorizedElements2 = BaseElements.of(items, null);
        ValidationResult result2 = categorizedElements2.validateRequire();
        Assert.assertTrue(result2.isValid());

        items.add(categorizedElement1);
        BaseElements categorizedElements3 = BaseElements.of(items, Override.class);
        ValidationResult result3 = categorizedElements3.validateRequire();
        Assert.assertTrue(result3.isValid());
    }

    @Test
    public void testValidateOneOrLess() {
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, null, null);
        List<BaseElement> items = new ArrayList<>();

        BaseElements categorizedElements1 = BaseElements.of(items, null);
        ValidationResult result = categorizedElements1.validateTargetFieldOneOrLess();
        Assert.assertTrue(result.isValid());

        items.add(categorizedElement1);
        BaseElements categorizedElements2 = BaseElements.of(items, null);
        ValidationResult result2 = categorizedElements2.validateTargetFieldOneOrLess();
        Assert.assertTrue(result2.isValid());

        items.add(categorizedElement1);
        BaseElements categorizedElements3 = BaseElements.of(items, Override.class);
        ValidationResult result3 = categorizedElements3.validateTargetFieldOneOrLess();
        Assert.assertFalse(result3.isValid());
    }

    @Test
    public void testHashCode() {
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, null, null);
        List<BaseElement> items = new ArrayList<>();
        items.add(categorizedElement1);
        BaseElements categorizedElements1 = BaseElements.of(items, null);
        BaseElements categorizedElements2 = BaseElements.of(items, null);
        Assert.assertEquals(categorizedElements1.hashCode(), categorizedElements2.hashCode());
    }

    @Test
    public void testEquals() {
        BaseElement categorizedElement1 = BaseElement.of(this.processingEnv, null, null);
        List<BaseElement> items = new ArrayList<>();
        items.add(categorizedElement1);
        BaseElements categorizedElements1 = BaseElements.of(items, null);
        BaseElements categorizedElements2 = BaseElements.of(items, null);
        Assert.assertTrue(categorizedElements1.equals(categorizedElements1));
        Assert.assertTrue(categorizedElements1.equals(categorizedElements2));
        Assert.assertFalse(categorizedElements1.equals(null));
        Assert.assertFalse(categorizedElements1.equals(1));
    }
}
