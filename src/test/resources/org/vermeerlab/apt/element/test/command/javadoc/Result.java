// Copyright 9999  LICENSE-OWNER-NAME-GenerateCodeTestCommand
// org.vermeerlab.apt.element.test.command.javadoc.GenerateJavaDocCommand
//
package org.vermeerlab.apt.element.test.command.javadoc;

import javax.annotation.Generated;

/**
 * Class Detail Comment-GenerateCodeTestCommand "AAAAA"
 *
 * @since 1.0
 * @since 1.0-GenerateCodeTestCommand
 * @version 1.1
 * @version 1.1-GenerateCodeTestCommand
 * @author AUTHOR-NAME
 * @author AUTHOR-NAME-GenerateCodeTestCommand
 */
@Generated({"org.vermeerlab.apt.AnnotationProcessorController", "org.vermeerlab.apt.element.test.command.javadoc.GenerateJavaDocCommand"})
class Result {
}
