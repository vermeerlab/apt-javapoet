Annotation Processer with JavaPoet
===========================================================

Annotation Processer によりJavaコードを生成するためのライブラリです。

## Description

アノテーション注釈要素の情報を使用して JavaCodeを生成します.

## Requirement
事前にローカルリポジトリに `parentpom` プロジェクトを作成してください。

`git clone https://bitbucket.org/vermeerlab/parentpom.git`


## Usage
Maven Repository

https://github.com/vermeerlab/maven/tree/mvn-repo/org/vermeerlab/annotation-processor-javapoet

## Version
0.1.0
初回リリース


## LICENSE
Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Author
[_vermeer_](https://twitter.com/_vermeer_)